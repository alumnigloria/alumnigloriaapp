import firebase from "@react-native-firebase/app";
import '@react-native-firebase/firestore';

import {
  API_KEY,
  AUTH_DOMAIN,
  DATABASE_URL,
  PROJECT_ID,
  STORAGE_BUCKET,
  MESSAGING_SENDER_ID,
  APP_ID,
  MEASUREMENT_ID
} from "react-native-dotenv";

const config = {
  apiKey: API_KEY,
  authDomain: AUTH_DOMAIN,
  databaseURL: DATABASE_URL,
  projectId: PROJECT_ID,
  storageBucket: STORAGE_BUCKET,
  messagingSenderId: MESSAGING_SENDER_ID,
  appId: APP_ID,
  measurementId: MEASUREMENT_ID
}

if (!firebase.apps.length) {
  firebase.initializeApp(config);
  firebase.firestore().settings({ experimentalForceLongPolling: true });
}

export const firestore = firebase.firestore();
export const FB_URL = 'https://storage.googleapis.com/gloria-alumni-app.appspot.com/';
export default firebase;

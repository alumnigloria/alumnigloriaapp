/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component, Fragment } from 'react';
import { AppState, InteractionManager, Platform, SafeAreaView, StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import MainStack from '@elements/mainNavigation';
import { dstyles } from '@handler/data';
import Reducers from '@handler/reducerHandler';
import auth from '@react-native-firebase/auth';

const store = createStore(Reducers);

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUserId: undefined,
      client: undefined,
      appState: AppState.currentState,
      profile: null,
      loading: true,
      // Set an initializing state whilst Firebase connects
      initializing: true,
      user: undefined,
    };

    const onAuthStateChanged = (user) => {
      this.setState({user: user});
      if (this.state.initializing) this.setState({initializing: false});
    }

      async function subscriber() {
      //AppState.addEventListener('change', this._handleAppStateChange);
      const subscriber = await auth().onAuthStateChanged(onAuthStateChanged);
      return subscriber; // unsubscribe on unmount
    }

    subscriber();

    if (this.state.initializing) return null;
  }

  // After the app loaded
  componentDidMount() {
    const _setTimeout = global.setTimeout;
    const _clearTimeout = global.clearTimeout;
    const MAX_TIMER_DURATION_MS = 60 * 1000;

    if (Platform.OS === 'android') {
      // Work around issue `Setting a timer for long time`
      // see: https://github.com/firebase/firebase-js-sdk/issues/97
      const timerFix = {};
      const runTask = (id, fn, ttl, args) => {
        const waitingTime = ttl - Date.now();

        if (waitingTime <= 1) {
          InteractionManager.runAfterInteractions(() => {
            if (!timerFix[id]) {
              return;
            }
            delete timerFix[id];
            fn(...args);
          });
          return;
        }

        const afterTime = Math.min(waitingTime, MAX_TIMER_DURATION_MS);
        timerFix[id] = _setTimeout(() => runTask(id, fn, ttl, args), afterTime);
      };

      global.setTimeout = (fn, time, ...args) => {
        if (MAX_TIMER_DURATION_MS < time) {
          const ttl = Date.now() + time;
          const id = '_lt_' + Object.keys(timerFix).length;

          runTask(id, fn, ttl, args);
          return id;
        }
        return _setTimeout(fn, time, ...args);
      };

      global.clearTimeout = id => {
        if (typeof id === 'string' && id.startsWith('_lt_')) {
          _clearTimeout(timerFix[id]);
          delete timerFix[id];
          return;
        }
        _clearTimeout(id);
      };
    }

    // Firebase Auth

    // Handle user state changes
    // TypeError: undefined is not an object (evaluating 'this.setState') use const instead of function that has setState() inside the func

    // useEffect(() => {
    //   const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    //   return subscriber; // unsubscribe on unmount
    // }, []);


    //   this.setState({loading: false});
    //   AppState.addEventListener('change', this._handleAppStateChange);
    // }

    // _handleAppStateChange = (nextAppState) => {
    //   if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
    //   }
    //   this.setState({appState: nextAppState});
  }
  // console.reportErrorsAsExceptions = false;

  componentWillUnmount() {
    // if (this.state.client != undefined) {
    //   this.state.client.close();
    // }
    // console.log('App closed!');
    // AppState.removeEventListener('change', this._handleAppStateChange);
  }


  render() {
    let loginStatus = "Currently Logged Out";
    if (this.state.currentUserId) {
      loginStatus = `Currently logged in as ${this.state.currentUserId}!`;
    }

    return (
      <Fragment>
        {Platform.OS === 'ios' && <StatusBar barStyle="dark-content" />}
        <SafeAreaView style={dstyles.container}>
          <Provider store={store}>
            <MainStack {...this.props} state={this.state} loading={this.state.loading} />
          </Provider>
        </SafeAreaView>
      </Fragment>
    );
  }
};

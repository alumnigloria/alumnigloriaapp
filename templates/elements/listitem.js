import React from 'react';
import { ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Image, Text } from 'react-native-elements';
import { Color, Size, dstyles, label } from '@handler/data';

const ListItem = ({ title, desc = "", image = "", url = null, sapproved = false }) => {
  let img = typeof(image) == 'string' && image.includes('http') ? {uri: image} : image;
  let approved = sapproved != null ? <Text style={styles.desc}>{sapproved ? 'Approved' : 'Not yet approved'}</Text> : null;

  let item = <TouchableOpacity onPress={url} style={styles.item}>
    <Image source={img} style={styles.img} />
    <View style={styles.itemText}>
      <Text style={styles.title}>{title}</Text>
      <Text numberOfLines={3} ellipsizeMode='tail' style={styles.desc}>{desc}</Text>
      <Text style={styles.link}>{label.seemore}</Text>
    </View>
  </TouchableOpacity>;

  return item;
}

const ListItem2 = ({ title, image = "", url = null, sapproved = false }) => {
  let img = typeof(image) == 'string' && image.includes('http') ? {uri: image} : image;
  let approved = sapproved != null ? <Text style={styles.desc}>{sapproved ? 'Approved' : 'Not yet approved'}</Text> : null;

  let item = <TouchableOpacity onPress={url} style={styles.item2}>
    <Image source={img} style={styles.img} />
    <Text style={[styles.title, dstyles.textCenter, {marginTop: Size.xs}]}>{title}</Text>
  </TouchableOpacity>;

  return item;
}

const LogoItem = ({ image = "", url = null }) => {
  let img = typeof(image) == 'string' && image.includes('http') ? {uri: image} : image;

  let item = <TouchableOpacity onPress={url} style={styles.sponsorListItem}>
    <Image source={img} style={styles.sponsorImg} />
  </TouchableOpacity>;

  return item;
}

export const CommunityListItem = ({ navigation, title = label.title, desc = label.loremipsum, image = null, options = [] }) => {
  let item = options && options['type'] == 2 ?
    <ListItem2 title={title} url={options.url} image={image} approved={options.showApproved} /> :
    <ListItem title={title} desc={desc} url={options.url} image={image} approved={options.showApproved} />;

  return item;
}

export const PromotionListItem = ({ navigation, title = label.title, desc = label.loremipsum, image = null, options = [] }) => {
  let item = options && options['type'] == 2 ?
    <ListItem2 title={title} url={options.url} image={image} approved={options.showApproved} /> :
    <ListItem title={title} desc={desc} url={options.url} image={image} approved={options.showApproved} />;

  return item;
}

export const SponsorListItem = ({ navigation, image = null, options = [] }) => {
  let item = <LogoItem image={image} url={options.url} />

  return item;
}

export const SearchListItem = ({ userId = null, name = '', click = null, center = false }) => {
  let item = <TouchableOpacity key={userId} color="black" type="clear" onPress={click}><Text style={[styles.listItem, center ? dstyles.textCenter : null]}>{name}</Text></TouchableOpacity>

  return item;
}

const styles = StyleSheet.create({
  item: {
    flex: 1,
    flexDirection: 'row',
    padding: Size.xs,
    paddingBottom: Size.sm,
  },
  item2: {
    width: 160,
    padding: Size.xs,
    paddingBottom: Size.xs,
  },
  sponsorListItem: {
    backgroundColor: Color.white,
    width: 50,
    height: 50,
    marginHorizontal: Size.xs,
    marginVertical: 9,
    borderRadius: 25,
    overflow: 'hidden',
  },
  itemText: {
    flex: 1,
    flexDirection: 'column',
    padding: Size.xs,
    paddingLeft: Size.md,
    backgroundColor: Color.white,
    height: 100,
  },
  sponsorImg: {
    width: 50,
    height: 50,
  },
  img: {
    width: 150,
    height: 100,
  },
  title: {
    fontSize:Size.fmedium,
    fontWeight:Size.fwbold,
    paddingBottom: Size.xs,
    flexWrap: 'wrap',
  },
  desc: {
    flex:1,
    fontSize:Size.fnormal,
    fontWeight:Size.fwnormal,
    overflow: 'hidden',
    flexWrap: 'wrap',
  },
  link: {
    color:Color.theme,
    fontSize:Size.fsmall,
    fontWeight:Size.fwnormal,
  },
  listItem: {
    alignSelf: 'stretch',
    width: "100%",
    paddingHorizontal: Size.sm,
    paddingVertical: Size.md,
    borderBottomWidth: 1,
    borderColor: Color.lightGrey,
  },
});

import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MainScreen from '@pages/main';
import LoadingScreen from '@pages/loading';
import LoginScreen from '@pages/login';
import PostersScreen from '@pages/posters';
import RegisterScreen from '@pages/register';
import CompanyLoginScreen from '@pages/company_login';
import CompanyRegisterScreen from '@pages/company_register';
import { setState, setStateInit, setStateUser } from '@actions/initActions';

const Stack = createStackNavigator();

function MainNavigation(props) {
  const params = props;
  const [init, setInit] = useState(false);

  useEffect(function() {
    if ((params.state.user || params.state.user === null) && !init) {
      setInit(true);
      props.setStateInit(false);
      props.setStateUser(params.state.user);
    }
  }, [params.state.user]);

  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none">
        <Stack.Screen name="Loading" component={LoadingScreen} />
        <Stack.Screen name="Posters" component={PostersScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="CompanyLogin" component={CompanyLoginScreen} />
        <Stack.Screen name="Register" component={RegisterScreen} />
        <Stack.Screen name="CompanyRegister" component={CompanyRegisterScreen} />
        <Stack.Screen name="Main" component={MainScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const mapStateToProps = (state) => {
  const { initRed } = state
  return { initRed }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    setState,
    setStateInit,
    setStateUser,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(MainNavigation);

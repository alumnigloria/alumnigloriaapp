import React, { useEffect } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, View, useWindowDimensions } from 'react-native';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CustomDrawer from '@elements/drawer';
import CommunityNavigation from '@elements/communityNavigation';
import ProfileNavigation from '@elements/profileNavigation';
import CompanyProfileNavigation from '@elements/companyProfileNavigation';
import PromotionNavigation from '@elements/promotionNavigation';
import SearchNavigation from '@elements/searchNavigation';
import MyEPNavigation from '@elements/myEPNavigation';
import { Color, dstyles, label } from '@handler/data';
import { icons } from '@handler/img';
import SettingsScreen from '@pages/settings';
import LogoutScreen from '@pages/logout';
import { ViewProfileScreen, ViewCompanyProfileScreen } from '@pages/view_profile';
import { setNavIndex } from '@actions/otherActions';


function MainDrawer(props) {
  const Drawer = createDrawerNavigator();
  const user = {firstname: 'Gabs', email:'gabrielandreas96@gmail.com'};
  const dimensions = useWindowDimensions();
  const isLargeScreen = dimensions.width >= 768;
  var index = props.otherRed.nav.index;

  const isCompany = false;

  useEffect(() => {
    if (props.route.state && index != props.route.state.index) {
      props.setNavIndex(props.route.state.index);
    }
  });

  // <Drawer.Screen name="Settings" component={SettingsScreen} />

  return (
      <Drawer.Navigator
        initialRouteName="Promotions"
        drawerType={isLargeScreen ? 'permanent' : 'front'}
        overlayColor="rgba(0,0,0,0.5)"
        drawerPosition="right"
        drawerContentOptions={{activeTintColor: Color.lightGrey}}
        drawerContent={(props) => {
          const filteredProps = {
            ...props,
            state: {
              ...props.state,
              routeNames: props.state.routeNames.filter(
                (routeName) => {
                  routeName !== 'ProfileScreen' && routeName !== 'CompanyProfileScreen' && routeName !== 'LogoutScreen';
                }
              ),
              routes: props.state.routes.filter(
                (route) => route.name !== 'ProfileScreen' && route.name !== 'CompanyProfileScreen' && route.name !== 'LogoutScreen'
              ),
            }
          }
          return <DrawerContentScrollView {...filteredProps}>
              <CustomDrawer {...filteredProps} {...user} />
            </DrawerContentScrollView>
        }}
        drawerContentOptions={{itemStyle:dstyles.sidebarItem}} >
        <Drawer.Screen name="PromotionNavigation" options={{title:"Promotions"}} component={PromotionNavigation} />
        <Drawer.Screen name="CommunityNavigation" options={{title:"Communities"}} component={CommunityNavigation} />
        { isCompany ? <Drawer.Screen name="ProfileNavigation" options={{title:"Profile", screen: "ViewOwnProfile"}} component={CompanyProfileNavigation} initialParams={{first: true}} /> : <Drawer.Screen name="ProfileNavigation" options={{title:"Profile", screen: "ViewOwnProfile"}} component={ProfileNavigation} initialParams={{first: true}} /> }
        <Drawer.Screen name="SearchNavigation" options={{title:"Search"}} component={SearchNavigation} />
        <Drawer.Screen name="MyEPNavigation" options={{title:"My Communities & Promotions"}} component={MyEPNavigation} />
        <Drawer.Screen name="ProfileScreen" options={{title:"Profile"}} component={ViewProfileScreen} initialParams={{first: true}} />
        <Drawer.Screen name="CompanyProfileScreen" options={{title:"Company Profile"}} component={ViewCompanyProfileScreen} />
        <Drawer.Screen name="LogoutScreen" component={LogoutScreen} />
      </Drawer.Navigator>
  );
}

const mapStateToProps = (state) => {
  const { otherRed } = state
  return { otherRed }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    setNavIndex,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(MainDrawer);

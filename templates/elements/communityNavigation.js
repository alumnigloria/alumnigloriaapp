import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import CommunitiesScreen from '@pages/communities';
import ViewCommunityScreen from '@pages/view_community';

const Stack = createStackNavigator();

export default function CommunityNavigation(props) {
  const params = props;

  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Communities" component={CommunitiesScreen} />
      <Stack.Screen name="ViewCommunity" component={ViewCommunityScreen} />
    </Stack.Navigator>
  );
}

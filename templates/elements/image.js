import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, Image } from 'react-native-elements';
import { img } from '@handler/img';
import { Color, Size, dstyles } from '@handler/data';

const BorderedImage = ({ source = null, circle = false, width = 0, height = 0 }) => {
  let img2 = typeof(source) == 'string' && source.includes('http') ? {uri: source} : source;
  const [val, setVal] = useState(img2);
  const [uploading, setUploading] = useState(false);
  const uploadServer = 'http://localhost:3000/api/upload';
  const desc = width > 0 || height > 0 ? <Text style={[dstyles.loading, {marginTop: 0, marginBottom: 0}]}>Recommended image size: {width} x {height}</Text> : null;

  const handleChoosePhoto = () => {
    const options = {noData: true};
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.uri) {
        try {
          if (handleUploadPhoto != null) {
            setUploading(true);
            if (handleUploadPhoto(response)) {
              setUploading(false);
              setVal({uri: response.uri});
            } else {
              setUploading(false);
            }
          }
        } catch(err) {
          console.log('Error: ' + err);
        }
      }
    });
  }

  const Image1 = ({ source, circle }) => (
    <View style={circle ? dstyles.circleFrame : dstyles.rectFrame}>
      <Image source={circle ? img.circle_frame : img.rect_frame} style={circle ? dstyles.imageCircleBack : dstyles.imageRectBack} />
      <View style={circle ? dstyles.imageCircleCont : dstyles.imageRectCont}>
      {
        uploading ?
        <View style={styles.progressBarContainer}>
          <Text style={[dstyles.loading, {marginBottom: 0}]}>{'Uploading Image...'}</Text>
        </View> :
        <Image source={img2} style={circle ? dstyles.imageCircle : dstyles.imageRect} />
      }
      </View>
    </View>
  );

  let input = <Image1 source={source} circle={circle} />
  return input;
}

const styles = StyleSheet.create({
});

export default BorderedImage;

import React, { useState } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { StackActions } from '@react-navigation/native';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItem, DrawerItemList } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { Color, Input, Size, dstyles, label } from '@handler/data';
import { icons } from '@handler/img';
import AsyncStorage from '@react-native-community/async-storage';

export default function CustomDrawer(props) {
  const navigation = props.navigation;
  const [msg, setMsg] = useState(false);

  function logout() {
    navigation.navigate('LogoutScreen');
  }

  return (
    <ScrollView style={styles.drawer}>
      <DrawerItemList {...props} />
      <DrawerItem
        label="Sign Out"
        style={dstyles.sidebarItem}
        containerStyle={{margin: 0, padding: 0}}
        onPress={logout} />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  drawer: {
    flex: 1,
    padding: 0
  },
});

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import PromotionsScreen from '@pages/promotions';
import ViewPromotionScreen from '@pages/view_promotion';

const Stack = createStackNavigator();

export default function PromotionNavigation(props) {
  const params = props;

  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Promotions" component={PromotionsScreen} />
      <Stack.Screen name="ViewPromotion" component={ViewPromotionScreen} />
    </Stack.Navigator>
  );
}

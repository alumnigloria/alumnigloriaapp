import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Image, Text } from 'react-native-elements';
import { img } from '@handler/img';
import { Color, Size, dstyles } from '@handler/data';

const Banner = ({ title, image, options = [] }) => {

  const PageBanner = ({ title, image }) => (
    <View style={[dstyles.bgTheme, styles.container]}>
      <Image source={image} style={styles.img} />
      <Text style={styles.title}>{title}</Text>
    </View>
  );

  let banner = <PageBanner title={title} image={image} />

  return banner;
}

const styles = StyleSheet.create({
  container: {
  },
  img: {
    minWidth: "100%",
    height: 110,
    opacity: 0.1,
    zIndex: 1,
  },
  title: {
    flex:1,
    position: 'absolute',
    color:Color.white,
    fontSize:Size.fxlarge,
    lineHeight: 29,
    paddingVertical: 25,
    top: 0,
    width: "100%",
    height: Size.pgBannerH,
    zIndex: 2,
    alignSelf: 'stretch',
    textAlign: 'center',
    textAlignVertical:'center'
  },
});

export default Banner;

import React, { useState } from 'react';
import { Dimensions, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { Color } from '@handler/data';


const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);


const Carous = ({data, nav, params = null}) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const navigation = nav;
  const layout = params && params['layout'] ? params['layout'] : 'default';
  const width = params && params['width'] ? params['width'] : SLIDER_WIDTH;
  const itemwidth = params && params['itemwidth'] ? params['itemwidth'] : ITEM_WIDTH;

  var car;

  const _renderItem = ({item, i}) => {
    var img = typeof(item.image) == 'string' && item.image.includes('http') ? {uri: item.image} : item.image;

    return (
      <TouchableOpacity style={styles.slide} onPress={item.url}>
        <Image source={img} style={styles.promo} />
      </TouchableOpacity>
    );
  }

  return (
    data.length > 0 ?
    <View style={styles.carouselCont}>
      <Carousel
        layout={layout} // stack, default, tinder
        //layoutCardOffset={9} //tinder = 9, stack = 18
        ref={car}
        data={data} renderItem={_renderItem}
        sliderWidth={width} itemWidth={itemwidth}
        //onLayout={() => {}}
        //onScroll={() => {}}
        //onBeforeSnapToItem={() => {}}
        onBeforeSnapToItem = { index => setActiveIndex(index) }
        //firstItem={2}
        //hasParallaxImages={true}
        //vertical={true}
        //loop={true}
        //loopClonesPerSide={3}
        //autoplay={true}
        //autoplayDelay={1000}
        //lockScrollWhileSnapping={true}
        swipeThreshold={5}
        inactiveSlideShift={0}
        useScrollView={true}
        //contentContainerCustomStyle={styles.carousel}
        containerCustomStyle={styles.carousel} />
      <Pagination
        dotsLength={data.length}
        activeDotIndex={activeIndex}
        //activeOpacity={1}
        //dotColor={Color.grey}
        //inactiveDotColor={Color.lightGrey}
        //inactiveDotElement={}
        inactiveDotOpacity={0.2}
        carouselRef={car}
        //tappableDots={true}
        //vertical={true}
        dotStyle={styles.dot}
        dotContainerStyle={styles.dotContainer}
        containerStyle={styles.dotContainer} />
    </View> : null
  );
}

const styles = StyleSheet.create({
  carouselCont: {
    alignItems: 'center',
  },
  slide: {
    color: Color.dark,
    backgroundColor: 'white',
    flex: 1,
    alignSelf: 'center',
    width: 240,
    height: 300,
    // ios
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.15,
    shadowRadius: 2,
    // android
    elevation: 3
  },
  dotContainer: {
    top: -5,
  },
  dot: {
    marginHorizontal: -3,
  },
  slideTitle: {
    padding: 22,
    paddingVertical: 15,
  },
  slideSubTitle: {
    padding: 22,
    paddingTop: 0,
    paddingBottom: 15,
    color: Color.lightGrey,
  },
  promo: {
    width: 240,
    height: 300,
  },
});

export default Carous;

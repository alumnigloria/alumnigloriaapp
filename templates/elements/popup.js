import React, { useRef } from 'react';
import { Animated, ScrollView, StyleSheet, TextInput, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements'; // styling see at react-native-elements doc
import { Color, Input, Size, dstyles, label } from '@handler/data';
import { img } from '@handler/img';


const Popup = ({ title, text, buttonText = label.ok, show = false, callback = null }) => {
  const fadeAnim = useRef(new Animated.Value(0)).current;
  React.useEffect(() => {
    Animated.timing(
      fadeAnim, { toValue: 1, duration: 200, useNativeDriver: true }
    ).start();
  }, [fadeAnim]);

  return (
    show ?
    <Animated.View style={[styles.centeredView, {opacity: fadeAnim}]}>
      <View style={styles.modalView}>
        <View style={styles.content}>
          { title ? <Text style={[styles.title, dstyles.textCenter]}>{title}</Text> : null}
          { text ? <Text style={[dstyles.text, dstyles.textCenter]}>{text}</Text> : null}
          <View style={[dstyles.alignMiddle, {'marginTop':20}]}>
            <Button type="solid" title={buttonText} buttonStyle={[dstyles.button, dstyles.bgSecondary]} onPress={callback} />
          </View>
        </View>
      </View>
    </Animated.View> : null
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    zIndex: 100,
    backgroundColor: 'rgba(0,0,0,.5)',
  },
  modalView: {
    margin: Size.lg,
    flexDirection: 'row',
    backgroundColor: "white",
    borderRadius: Size.rad,
    paddingHorizontal: 20,
    paddingTop: 20,
    paddingBottom: 8,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  content: {
    flex: 1,
    width: '100%',
  },
  title: {
    fontSize: Size.flarge,
    alignSelf: 'stretch',
    textAlign: 'center',
    fontWeight: Size.fwbold,
    paddingTop: Size.xs,
    paddingBottom: Size.sm,
  },
});

export default Popup;

import React, { useState } from 'react';
import { Alert, StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';
import { Image, Text } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
// import { Picker } from '@react-native-community/picker';
import RNPickerSelect from 'react-native-picker-select';
import * as Progress from 'react-native-progress';
import { icons, img } from '@handler/img';
import { Color, Size, Input, dstyles } from '@handler/data';

export const ImageInput = ({ title, source = null, circle = false, type = 1, width = 0, height = 0, handleUploadPhoto = null }) => {
  let img2 = typeof(source) == 'string' && source.includes('http') ? {uri: source} : source;
  const [val, setVal] = useState(img2);
  const [uploading, setUploading] = useState(false);
  const uploadServer = 'http://localhost:3000/api/upload';
  const desc = width > 0 || height > 0 ? <Text style={[dstyles.loading, {marginTop: 0, marginBottom: 0}]}>Recommended image size: {width} x {height}</Text> : null;

  const handleChoosePhoto = async () => {
    const options = {noData: true};
    await ImagePicker.launchImageLibrary(options, async (response) => {
    console.log("TESTT");
      if (response.uri) {
        try {
          if (await handleUploadPhoto != null) {
            // if upload success
            setUploading(true);
            if (handleUploadPhoto(response)) {
              //Alert.alert('Photo uploaded!', 'Your photo has been uploaded!');
              setUploading(false);
              console.log(response);
              setVal({uri: response.uri});
            } else {
              //Alert.alert('Photo upload failed!');
              setUploading(false);
            }
          }
        } catch(err) {
          console.log('Error: ' + err);
        }
      }
    });
    // const options = {
    //   maxWidth: 2000,
    //   maxHeight: 2000,
    //   storageOptions: {
    //     skipBackup: true,
    //     path: 'images'
    //   }
    // };
    // ImagePicker.showImagePicker(options, response => {
    //   if (response.didCancel) {
    //     console.log('User cancelled image picker');
    //   } else if (response.error) {
    //     console.log('ImagePicker Error: ', response.error);
    //   } else if (response.customButton) {
    //     console.log('User tapped custom button: ', response.customButton);
    //   } else {
    //     const source = { uri: response.uri };
    //     console.log(source);
    //     setImage(source);
    //   }
    // });
  }

  //const Input1 = ({ title, source, circle, type }) => ();
  // <Progress.Bar progress={transferred} width={300} />

  let imgPicker =
    uploading ?
    <View style={styles.progressBarContainer}>
      <Text style={[dstyles.loading, {marginBottom: 0}]}>{'Uploading Image...'}</Text>
    </View> :
    <TouchableOpacity color="black" type="clear" style={circle ? dstyles.circleFrame : dstyles.rectFrame} onPress={handleChoosePhoto}>
      <Image source={circle ? img.circle_frame : img.rect_frame} style={circle ? dstyles.imageCircleBack : dstyles.imageRectBack} />
      <View style={circle ? dstyles.imageCircleCont : dstyles.imageRectCont}>
        <Image source={val} style={circle ? dstyles.imageCircle : dstyles.imageRect} />
        <View style={styles.alignMiddle}>
          <Text>{'Click to change image'}</Text>
        </View>
        <Image source={img.edit} style={dstyles.editIcon} />
      </View>
    </TouchableOpacity>;

  let input = <View>
    <Text style={type == 1 ? dstyles.label : [dstyles.label, dstyles.textWhite]}>{title}</Text>
    { desc }
    { imgPicker }
  </View>

  //let input = <Input1 title={title} source={source} circle={circle} type={type} />
  return input;
}

export const LabelInput = ({ title, value = "", placeholder = "", desc="", inputType = Input.default, type = 1, inline = false, onChange = null, secureTextEntry = false }) => {
  const [val, setVal] = useState(value);
  var labelStyle = [dstyles.label], inputStyle = [dstyles.input];

  if (type == 2) {
    labelStyle.push(dstyles.textWhite);
    inputStyle.push(dstyles.textCenter);
  }
  if (inline) {
    labelStyle.push(styles.inlineLabel);
    inputStyle.push(styles.inlineInput);
  }
  let description = desc ? <Text style={[dstyles.loading, {marginTop: 0, marginBottom: Size.xs}]}>{desc}</Text> : null;

  let input = <View style={inline ? styles.inline : {flex: 1, minHeight: 80}}>
    <Text style={labelStyle}>{title}</Text>
    { description }
    <TextInput style={inputStyle}
      value={onChange != null ? value : val}
      placeholder={placeholder}
      placeholderTextColor={Color.lightGrey}
      textContentType={inputType}
      secureTextEntry={secureTextEntry}
      onChangeText = {onChange != null ? onChange : e => setVal(e)}
      maxLength={200} />
  </View>
  return input;
}

export const TextAreaInput = ({ title, value = "", placeholder = "", inputType = Input.default, type = 1, onChange = null }) => {
  const [val, setVal] = useState(value);
  let input = <View>
    <Text style={type == 1 ? dstyles.label : [dstyles.label, dstyles.textWhite]}>{title}</Text>
    <TextInput style={dstyles.textarea}
      defaultValue={value}
      placeholder={placeholder}
      placeholderTextColor={Color.lightGrey}
      textContentType={inputType}
      maxLength={200}
      multiline={true}
      onChangeText = {onChange != null ? onChange : e => setVal(e)}
      numberOfLines={5} />
  </View>
  return input;
}

export const PickerInput = ({ title, values = [], placeholder = "", type = 1, inline = false, onChange = null, selectedValue = null }) => {
  var labelStyle = [dstyles.label], inputStyle = [styles.picker];
  if (type == 2) {
    labelStyle.push(dstyles.textWhite);
    inputStyle.push(dstyles.textCenter);
  }
  if (inline) {
    labelStyle.push(styles.inlineLabel);
    inputStyle.push(styles.inlineInput);
  }

  let input = <View style={inline ? styles.inline : ''}>
    <Text style={labelStyle}>{title}</Text>
    <View style={[dstyles.input, inputStyle]}>
      <RNPickerSelect style={type == 1 ? dstyles.input : [dstyles.input, dstyles.textCenter, {marginBottom: 0, height: 38}]} containerStyle={{backgroundColor: 'white'}} itemStyle={{textAlign: 'center'}}
        selectedValue={selectedValue != null ? selectedValue : values[0].value}
        onValueChange={onChange} items={values} />
    </View>
    <View style={styles.selectArrowCont}>
      <Image source={icons.select_arrow} style={styles.selectArrow} />
    </View>
  </View>;
  return input;
}

const styles = StyleSheet.create({
  alignMiddle: {
    position: 'absolute',
    top: 5,
    left: 14,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    width: '100%',
    height: 110,
    zIndex: 2,
    backgroundColor: Color.transparent,
    opacity: 0,
  },
  inline: {
    flexDirection: 'row',
  },
  inlineLabel: {
    width: 84,
    textAlign: 'left',
    paddingTop: Size.sm,
    paddingBottom: Size.sm,
  },
  inlineInput: {
    textAlign: 'left',
  },
  picker: {
    borderRadius: Size.rad,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: Color.grey,
    marginBottom: Size.sm,
    flex: 1,
    height: 38,
    alignSelf: 'stretch',
  },
  selectArrowCont: {
    position: 'absolute',
    top:16,
    right: 20,
    zIndex: 30,
  },
  selectArrow: {
    width: 12,
    height: 8,
  }
});

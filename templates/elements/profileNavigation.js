import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { EditProfileScreen } from '@pages/edit_profile';
import { ViewOwnProfileScreen } from '@pages/view_profile';
import AddJobScreen from '@pages/add_job';

const Stack = createStackNavigator();

export default function ProfileNavigation(props) {
  const params = props;

  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="ViewOwnProfile" component={ViewOwnProfileScreen} />
      <Stack.Screen name="EditProfile" component={EditProfileScreen} />
      <Stack.Screen name="AddJob" component={AddJobScreen} />
    </Stack.Navigator>
  );
}

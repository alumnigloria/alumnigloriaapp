import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MyEPScreen from '@pages/myep';
import EditCommunityScreen from '@pages/edit_community';
import EditPromotionScreen from '@pages/edit_promotion';

const Stack = createStackNavigator();

export default function myEPNavigation(props) {
  const params = props;

  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="MyEP" component={MyEPScreen} />
      <Stack.Screen name="EditCommunity" component={EditCommunityScreen} />
      <Stack.Screen name="EditPromotion" component={EditPromotionScreen} />
    </Stack.Navigator>
  );
}

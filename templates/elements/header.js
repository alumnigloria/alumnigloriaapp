import React from 'react';
import { StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';
import { Image, Text } from 'react-native-elements';
import { icons } from '@handler/img';
import { Color } from '@handler/data';

const CustomHeader = ({ navigation, title, logo, options = [] }) => {

  const back = options && options['back'] != null ? options['back'] : false;
  const menu = options && options['menu'] != null ? options['menu'] : true;

  const backButton = back ?
    <TouchableOpacity color="black" type="clear" style={styles.backButton} onPress={() => navigation.goBack() }>
      <Image source={icons.left_white} style={styles.leftIcon} />
    </TouchableOpacity> : null;

  const menuButton = menu ?
    <TouchableOpacity color="black" type="clear" style={styles.menuButton} onPress={() => navigation.openDrawer() }>
      <Image source={icons.menu} style={styles.menuIcon} />
    </TouchableOpacity> : null;

  const PageHeader = ({ navigation, title, logo }) => (
    <View style={styles.container}>
      { backButton }
      <Text style={styles.title}>{title}</Text>
      { menuButton }
    </View>
  );

  let header = <PageHeader navigation={navigation} title={title} logo={logo} />

  return header;
}

const styles = StyleSheet.create({
  container: {
    backgroundColor:'#333',
    flexDirection:'row',
    padding:10
  },
  logo: {
    width: 30,
    height: 30,
    margin: 1,
    marginRight:15,
  },
  title: {
    flex:1,
    paddingVertical: 5,
    color:'#fff',
    fontSize:18,
    textAlignVertical:'center'
  },
  button: {
    width: 34,
    height: 34,
    marginLeft: 6
  },
  icon: {
    width: 30,
    height: 30
  },
  iconSmall: {
    width: 22,
    height: 22,
    margin: 6
  },
  leftIcon: {
    width: 13,
    height: 22,
    margin: 6,
    marginLeft: 1
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 7
  },
  backButton: {
    width: 40,
    height: 34,
    marginLeft: 6
  },
  menuIcon: {
    width: 26,
    height: 24,
    margin: 5
  }
});

export default CustomHeader;

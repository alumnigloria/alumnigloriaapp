import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SearchScreen from '@pages/search';
import SearchDetailsScreen from '@pages/search_details';

const Stack = createStackNavigator();

export default function SearchNavigation(props) {
  const params = props;

  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Search" component={SearchScreen} />
      <Stack.Screen name="SearchDetails" component={SearchDetailsScreen} />
    </Stack.Navigator>
  );
}

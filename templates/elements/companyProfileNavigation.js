import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { EditCompanyProfileScreen } from '@pages/edit_profile';
import { ViewCompanyProfileScreen } from '@pages/view_profile';

const Stack = createStackNavigator();

export default function ProfileNavigation(props) {
  const params = props;

  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="ViewCompanyProfile" component={ViewCompanyProfileScreen} />
      <Stack.Screen name="EditCompanyProfile" component={EditCompanyProfileScreen} />
    </Stack.Navigator>
  );
}

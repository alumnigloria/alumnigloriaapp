import React, { useState } from 'react';
import { ScrollView, StyleSheet, TextInput, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements';
import { StackActions } from '@react-navigation/native';
import Popup from '@elements/popup';
import { Color, Input, Size, dstyles, label } from '@handler/data';
import { img } from '@handler/img';


export default function BasicLayout(props) {
  const style = props.style ? props.style : null;
  const height = props.height ? props.height : 0;
  const title = props.title ? props.title : null;
  const [msg, setMsg] = useState(props.msg ? props.msg : false);
  const callback = () => {
    setMsg(false);
    if (props.callback != null) {
      props.callback();
    }
  }
  if (props.msg && !msg) { setMsg(props.msg); }

  const pop = msg != false ? <Popup title={title} text={msg} show={msg != false} callback={callback}></Popup> : null;

  return (
    height > 0 && Size.screenHeight < height ?
      <View style={style}>
        { pop }
        <ScrollView style={style}>
          { props.children }
        </ScrollView>
      </View>
    : <View style={style}>
        { pop }
        <ScrollView style={props.full ? {height: Size.screenHeight} : {height: height}}>
          { props.children }
        </ScrollView>
      </View>
  );
}

const styles = StyleSheet.create({
});

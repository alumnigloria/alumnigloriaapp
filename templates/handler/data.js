import { colors, sizes } from '@data/designs';
import { text } from '@data/text';
import { autoComplete, input, inputTypes, keyboard } from '@data/types';
import defstyles from '@data/dstyles';

export const Color = colors;
export const Size = sizes;
export const dstyles = defstyles;
export const label = text;
export const Input = input;
export const InputType = inputTypes;
export const Keyboard = keyboard;
export const AutoComplete = autoComplete;

const menu = require('@icons/menu.png');
const left_white = require('@icons/left_white.png');
const search = require('@icons/search.png');
const select_arrow = require('@icons/select_arrow.png');
const settings = require('@icons/settings.png');

const arrow = require('@img/arrow.png');
const back = require('@img/back.jpeg');
const back1 = require('@img/back1.jpg');
const back2 = require('@img/back2.jpg');
const circle_frame = require('@img/circle_frame.png');
const empty = require('@img/empty.png');
const empty200 = require('@img/empty200.png');
const empty600 = require('@img/empty600.png');
const empty_add = require('@img/empty_add.png');
const empty_user = require('@img/empty_user.png');
const header = require('@img/header.jpg');
const logo = require('@img/gloria_logo.png');
const logo_white = require('@img/gloria_logo.png');
const app_logo = require('@img/logo.png');
const promo1 = require('@img/promo1.jpg');
const rect_frame = require('@img/rect_frame.png');
const qr_code = require('@img/qrcode.png');
const woman = require('@img/woman.jpg');

export const icons = {
  menu,
  left_white,
  search,
  select_arrow,
  settings,
}

export const img = {
  app_logo,
  arrow,
  back,
  back1,
  back2,
  circle_frame,
  empty,
  empty200,
  empty600,
  empty_add,
  empty_user,
  header,
  logo,
  logo_white,
  promo1,
  rect_frame,
  qr_code,
  woman,
}

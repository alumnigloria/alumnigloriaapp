import { combineReducers } from 'redux';
import { label } from '@handler/data';

const data = {
  nav: {index: 0 },
}

const otherReducer = (state = data, action) => {
  const { nav } = state;
  switch (action.type) {
    case 'INDEX':
      nav.index = action.payload;
      break;
    default:
      break;
  }
  return { nav };
}

export default otherReducer;

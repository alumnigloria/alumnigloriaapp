import { label } from '@handler/data';

const data = {
  job: {name: '', company: '', start_year: '', end_year: ''/*business_id: '', start_date: '', end_date: ''*/},
};

export default jobReducer = (state = data, action) => {
  const { job, types } = state;
  switch (action.type) {
    case 'JOB_RESET':
      job.name = '';
      job.company = '';
      job.start_year = '';
      job.end_year = '';
      // job.business_id = '';
      // job.start_date = '';
      // job.end_date = '';
      break;
    case 'JOB':
      job.name = action.payload.name;
      job.company = action.payload.company;
      job.start_year = action.payload.start_year;
      job.end_year = action.payload.end_year;
      // job.business_id = action.payload.business_id;
      // job.start_year = action.payload.start_date;
      // job.end_year = action.payload.end_date;
      break;
    case 'JOB_NAME':
      job.name = action.payload;
      break;
    case 'JOB_COMPANY':
      job.company = action.payload;
      break;
    case 'JOB_START_YEAR':
      job.start_year = action.payload;
      break;
    case 'JOB_END_YEAR':
      job.end_year = action.payload;
      break;
    // case 'JOB_BUSINESS':
    //   job.business_id = action.payload;
    //   break;
    // case 'JOB_START_DATE':
    //   job.start_date = action.payload;
    //   break;
    // case 'JOB_END_DATE':
    //   job.end_date = action.payload;
    //   break;
    default:
        break;
  }
  return { job, types };
};

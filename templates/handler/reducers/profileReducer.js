import { label } from '@handler/data';

const data = {
  profile: {name: '', first_name: '', last_name: '', image: '', email: '', occupation: '', graduation: '', address: '', telp: '', facebook: '', instagram: '', linkedin: '', jobs: [] },
  cprofile: {name: '', image: '', email: '', occupation: '', year: '', address: '', telp: '', facebook: '', instagram: '', linkedin: '' },
};

export const ProfileRed = (state = data, action) => {
  const { profile, types } = state;
  switch (action.type) {
    case 'PROFILE_RESET':
      profile.name = '';
      profile.first_name = '';
      profile.last_name = '';
      profile.profpic = '';
      profile.email = '';
      profile.occupation = '';
      profile.graduation = '';
      profile.address = '';
      profile.facebook = '';
      profile.instagram = '';
      profile.linkedin = '';
      profile.jobs = [];
      break;
    case 'PROFILE':
      profile.name = action.payload.name;
      profile.first_name = action.payload.first_name;
      profile.last_name = action.payload.last_name;
      profile.profpic = action.payload.profpic;
      profile.email = action.payload.email;
      profile.occupation = action.payload.occupation;
      profile.graduation = action.payload.graduation;
      profile.address = action.payload.address;
      profile.facebook = action.payload.facebook;
      profile.instagram = action.payload.instagram;
      profile.linkedin = action.payload.linkedin;
      profile.jobs = action.payload.jobs;
      break;
    case 'PROFILE_NAME':
      profile.name = action.payload;
      break;
    case 'PROFILE_FIRSTNAME':
      profile.first_name = action.payload;
      break;
    case 'PROFILE_LASTNAME':
      profile.last_name = action.payload;
      break;
    case 'PROFILE_IMAGE':
      profile.profpic = action.payload;
      break;
    case 'PROFILE_EMAIL':
      profile.email = action.payload;
      break;
    case 'PROFILE_OCCUPATION':
      profile.occupation = action.payload;
      break;
    case 'PROFILE_GRADUATION':
      profile.graduation = action.payload;
      break;
    case 'PROFILE_ADDRESS':
      profile.address = action.payload;
      break;
    case 'PROFILE_TELP':
      profile.telp = action.payload;
      break;
    case 'PROFILE_FACEBOOK':
      profile.facebook = action.payload;
      break;
    case 'PROFILE_INSTAGRAM':
      profile.instagram = action.payload;
      break;
    case 'PROFILE_LINKEDIN':
      profile.linkedin = action.payload;
      break;
    case 'PROFILE_JOBS':
      profile.jobs = action.payload;
      break;
    default:
      break;
  }
  return { profile, types };
};

export const CProfileRed = (state = data, action) => {
  const { cprofile, types } = state;
  switch (action.type) {
    case 'PROFILE_RESET':
      cprofile.name = '';
      cprofile.profpic = '';
      cprofile.email = '';
      cprofile.occupation = '';
      cprofile.year = '';
      cprofile.address = '';
      cprofile.facebook = '';
      cprofile.instagram = '';
      cprofile.linkedin = '';
      break;
    case 'PROFILE':
      cprofile.name = action.payload.name;
      cprofile.profpic = action.payload.profpic;
      cprofile.email = action.payload.email;
      cprofile.occupation = action.payload.occupation;
      cprofile.year = action.payload.year;
      cprofile.address = action.payload.address;
      cprofile.facebook = action.payload.facebook;
      cprofile.instagram = action.payload.instagram;
      cprofile.linkedin = action.payload.linkedin;
      break;
    case 'PROFILE_NAME':
      cprofile.name = action.payload;
      break;
    case 'PROFILE_IMAGE':
      cprofile.profpic = action.payload;
      break;
    case 'PROFILE_EMAIL':
      profile.email = action.payload;
      break;
    case 'PROFILE_OCCUPATION':
      cprofile.occupation = action.payload;
      break;
    case 'PROFILE_YEAR':
      cprofile.graduation = action.payload;
      break;
    case 'PROFILE_ADDRESS':
      cprofile.address = action.payload;
      break;
    case 'PROFILE_TELP':
      cprofile.telp = action.payload;
      break;
    case 'PROFILE_FACEBOOK':
      cprofile.facebook = action.payload;
      break;
    case 'PROFILE_INSTAGRAM':
      cprofile.instagram = action.payload;
      break;
    case 'PROFILE_LINKEDIN':
      cprofile.linkedin = action.payload;
      break;
    default:
      break;
  }
  return { cprofile, types };
};

import { combineReducers } from 'redux';
import { label } from '@handler/data';

const data = {
  promo: {name: '', image: '', contact: '', details: '', url: '', start_time: '', end_time: '', is_active: false, is_approved: false, is_featured: false, location: '', owner: '' },
};

const promoReducer = (state = data, action) => {
  const { promo, types } = state;
  switch (action.type) {
    case 'PROMO_RESET':
      promo.name = '';
      promo.image = '';
      promo.contact = '';
      promo.details = '';
      promo.url = '';
      promo.location = '';
      promo.start_time = '';
      promo.end_time = '';
      break;
    case 'PROMO':
      promo.name = action.payload.name;
      promo.image = action.payload.image;
      promo.contact = action.payload.contact;
      promo.details = action.payload.details;
      promo.url = action.payload.url;
      promo.location = action.payload.location;
      promo.start_time = action.payload.start_time;
      promo.end_time = action.payload.end_time;
      break;
    case 'PROMO_NAME':
      promo.name = action.payload;
      break;
    case 'PROMO_IMAGE':
      promo.image = action.payload;
      break;
    case 'PROMO_CONTACT':
      promo.contact = action.payload;
      break;
    case 'PROMO_DETAILS':
      promo.details = action.payload;
      break;
    case 'PROMO_URL':
      promo.url = action.payload;
      break;
    case 'PROMO_LOCATION':
      promo.location = action.payload;
      break;
    case 'PROMO_START_TIME':
      promo.start_time = action.payload;
      break;
    case 'PROMO_END_TIME':
      promo.end_time = action.payload;
      break;
    default:
      break;
  }
  return { promo, types };
};

export default promoReducer;

import { combineReducers } from 'redux';
import { label } from '@handler/data';

const data = {
  community: {name: '', image: '', contact: '', details: '', url: '', start_time: '', end_time: '', is_active: false, is_approved: false, is_featured: false, location: '', owner: '' },
};

const communityReducer = (state = data, action) => {
  const { community, types } = state;
  switch (action.type) {
    case 'COMMUNITY_RESET':
      community.name = '';
      community.image = '';
      community.contact = '';
      community.details = '';
      community.url = '';
      community.location = '';
      community.start_time = '';
      community.end_time = '';
      break;
    case 'COMMUNITY':
      community.name = action.payload.name;
      community.image = action.payload.image;
      community.contact = action.payload.contact;
      community.details = action.payload.details;
      community.url = action.payload.url;
      community.location = action.payload.location;
      community.start_time = action.payload.start_time;
      community.end_time = action.payload.end_time;
      break;
    case 'COMMUNITY_NAME':
      community.name = action.payload;
      break;
    case 'COMMUNITY_IMAGE':
      community.image = action.payload;
      break;
    case 'COMMUNITY_CONTACT':
      community.contact = action.payload;
      break;
    case 'COMMUNITY_DETAILS':
      community.details = action.payload;
      break;
    case 'COMMUNITY_URL':
      community.url = action.payload;
      break;
    case 'COMMUNITY_LOCATION':
      community.location = action.payload;
      break;
    case 'COMMUNITY_START_TIME':
      community.start_time = action.payload;
      break;
    case 'COMMUNITY_END_TIME':
      community.end_time = action.payload;
      break;
    default:
      break;
  }
  return { community, types };
};

export default communityReducer;

import { combineReducers } from 'redux';

const data = {
  init: {
    initializing: null,
    user: null,
  },
};

const initReducer = (state = data, action) => {
  const { init, types } = state;
  switch (action.type) {
    case 'STATE':
      init.initializing = action.payload.initializing;
      init.user = action.payload.user;
      break;
    case 'STATE_INIT':
      init.initializing = action.payload;
      break;
    case 'STATE_USER':
      init.user = action.payload;
      break;
    default:
      break;
  }
  return { init, types };
};

export default initReducer;

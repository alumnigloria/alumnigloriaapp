import { label } from '@handler/data';

const data = {
  user: {email: '', password: '', first_name: '', last_name: '', graduation: '', telp: ''},
  cuser: {email: '', password: '', name: '', year: 0, location: '', telp: ''},
};

export const userReducer = (state = data, action) => {
  const { user, types } = state;
  switch (action.type) {
    case 'USER_RESET':
        user.email = '';
        user.password = '';
        user.first_name = '';
        user.last_name = '';
        user.graduation = '';
        user.telp = '';
        break;
    case 'USER':
        user.email = action.payload.email;
        user.password = action.payload.password;
        user.first_name = action.payload.first_name;
        user.last_name = action.payload.last_name;
        user.graduation = action.payload.graduation;
        user.telp = action.payload.telp;
        break;
    case 'USER_EMAIL':
        user.email = action.payload;
        break;
    case 'USER_PASSWORD':
        user.password = action.payload;
        break;
    case 'USER_FIRST_NAME':
        user.first_name = action.payload;
        break;
    case 'USER_LAST_NAME':
        user.last_name = action.payload;
        break;
    case 'USER_GRADUATION':
        user.graduation = action.payload;
        break;
    case 'USER_TELP':
        user.telp = action.payload;
        break;
    default:
        break;
  }
  return { user, types };
};

export const cuserReducer = (state = data, action) => {
  const { cuser, types } = state;
  switch (action.type) {
    case 'USER_RESET':
        cuser.email = '';
        cuser.password = '';
        cuser.name = '';
        cuser.graduation = '';
        cuser.location = '';
        cuser.telp = '';
        break;
    case 'USER':
        cuser.email = action.payload.email;
        cuser.password = action.payload.password;
        cuser.name = action.payload.name;
        cuser.graduation = action.payload.graduation;
        cuser.location = action.payload.location;
        cuser.telp = action.payload.telp;
        break;
    case 'USER_EMAIL':
        cuser.email = action.payload;
        break;
    case 'USER_PASSWORD':
        cuser.password = action.payload;
        break;
    case 'USER_NAME':
        cuser.name = action.payload;
        break;
    case 'USER_YEAR':
        cuser.year = action.payload;
        break;
    case 'USER_LOCATION':
        cuser.location = action.payload;
        break;
    case 'USER_TELP':
        cuser.telp = action.payload;
        break;
    default:
        break;
  }
  return { cuser, types };
};

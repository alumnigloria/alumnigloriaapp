import { combineReducers } from 'redux';
import InitRed from '@reducers/initReducer';
import CommunityRed from '@reducers/communityReducer';
import { ProfileRed, CProfileRed } from '@reducers/profileReducer';
import PromoRed from '@reducers/promoReducer';
import SearchRed from '@reducers/searchReducer';
import OtherRed from '@reducers/otherReducer';
import { userReducer, cuserReducer } from '@reducers/userReducer';
import JobReducer from '@reducers/jobReducer';

export default combineReducers({
  initRed: InitRed,
  communityRed: CommunityRed,
  profileRed: ProfileRed,
  cprofileRed: CProfileRed,
  promoRed: PromoRed,
  searchRed: SearchRed,
  otherRed: OtherRed,
  userRed: userReducer,
  cuserRed: cuserReducer,
  jobRed: JobReducer,
});

// const rootReducer = (state, action) => {
//    // Clear all data in redux store to initial.
//    if(action.type === 'DESTROY_SESSION')
//       state = undefined;
//
//    return combineReducer(state, action);
// };
//export default combineReducer;

import React from 'react';
import storage from '@react-native-firebase/storage';
import firebase from "../../config/firebase";
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-community/async-storage';
import { Size, label } from '@handler/data';

const addUser = async (data) => {
  const docDbRef = firebase.firestore().collection('users');
  var docId = null, msg = '';

  if (data.email && data.password) {
    await auth()
      .createUserWithEmailAndPassword(data.email, data.password)
      .then(async (data2) => {
        console.log('User account created & signed in.');
        // data.refreshToken = data2.user.refreshToken; // fb unsupported
        data.email_verified = data2.user.emailVerified;
        data.uid = data2.user.uid;
        data.password = '';
        await docDbRef.add(data).then(docRef => {
          console.log('Creating data.');
          msg = label.registerSuccess;
          docId = docRef.id;
        }).catch(error => {
          console.log('Error: ' + error);
          if (error.code === 'auth/network-request-failed') {
            msg = "A network error has occurred, please try again.";
          } else msg = label.registerFailed;
        });
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          msg = 'That email address is already in use.';
        } else if (error.code === 'auth/invalid-email') {
          msg = 'That email address is invalid.';
        } else if (error.code === 'auth/weak-password') {
          msg = 'That password must contains 6 characters.';
        } else if (error.code === 'auth/network-request-failed') {
          msg = "A network error has occurred, please try again.";
        }
        console.error('Error: ' + error);
      });
    if (firebase.auth().currentUser) {
      await auth().signOut();
      console.log('User account signed out!');
    } else if (!msg) msg = 'There is a network problem.';
  } else msg = 'Email and password cannot be empty.';

  return [docId, msg];
}

const addBusiness = async (data) => {
  const docDbRef = firebase.firestore().collection('businesses');
  var docId = null, msg = '';

  if (data.email && data.password) {
    await auth()
      .createUserWithEmailAndPassword(data.email, data.password)
      .then(async (data2) => {
        console.log('Business account created & signed in.');
        // data.refreshToken = data2.user.refreshToken; // fb unsupported
        data.email_verified = data2.user.emailVerified;
        data.uid = data2.user.uid;
        data.password = '';
        await docDbRef.add(data).then(docRef => {
          console.log('Creating data.');
          msg = label.registerSuccess;
          docId = docRef.id;
        }).catch(error => {
          console.log('Error: ' + error);
          if (error.code === 'auth/network-request-failed') {
            msg = label.errorMsgNetwork;
          } else msg = label.registerFailed;
        });
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          msg = label.errorMsgEmailUsed;
        } else if (error.code === 'auth/invalid-email') {
          msg = label.errorMsgEmailInvalid;
        } else if (error.code === 'auth/weak-password') {
          msg = label.errorMsgPasswordReq2;
        } else if (error.code === 'auth/network-request-failed') {
          msg = label.errorMsgNetwork;
        }
        console.error('Error: ' + error);
      });
    if (firebase.auth().currentUser) {
      await auth().signOut();
      console.log('Business account signed out!');
    } else if (!msg) msg = 'There is a network problem.';
  } else msg = 'Email and password cannot be empty.';

  return [docId, msg];
}

const capitalize = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

/*
 * get a user
 */
const getUser = async(params = {userId: null, self: false}) => {
  var userData = {};

  if (params.self) { // if self
    const json_user_data = await AsyncStorage.getItem('logged_in_user');
    userData = json_user_data ? JSON.parse(json_user_data) : null;

  } else if (params.userId) { // to get a specific user
    const userRef = firebase.firestore().collection('users').doc(params.userId);
    const comRef = firebase.firestore().collection('businesses');
    const comSnapshot = await comRef.get();
    var jobs = [];

    await userRef.get().then((res) => {
      if (res.id) {
        const user = res.data();

        if (user.first_name) {
          userData = {
            id: res.id,
            name: capitalize(user.first_name) + ' ' + capitalize(user.last_name),
            first_name: capitalize(user.first_name),
            last_name: capitalize(user.last_name),
            profpic: user.profile_picture ? user.profile_picture : null,
            biography: user.biography,
            contact: user.contact_number,
            email: user.email,
            location: user.location,
            occupation: user.occupation,
            graduation: user.year_of_graduation.toString(),
            visibility: user.profile_preference,
            sponsor: user.is_sponsor,
            active: user.is_active,
            approved: user.is_approved,
            facebook: user.facebook_url,
            linkedin: user.linkedin,
            instagram: user.instagram,
          };
        }
      } else {
        console.log("Document does not exist!");
      }
    }).catch(error => {
      console.log("Error: " + error.message);
    });

    userData.profpic = userData.profpic ? await fburl(userData.profpic) : null;
    userData.jobs = await getUserJobs({userId: params.userId});
  }

  return userData;
}

/*
 * check if user exists by email
 */
const checkUserExists = async(params = {email: null, type: null}) => {
  if (params.email && params.type) {
    const userRef = firebase.firestore().collection(params.type).where('email', '==', params.email);
    var userData = [];

    await userRef.get().then(userSnapshot => {

      userSnapshot.forEach(res => {
        const user = res.data();
        userData.push({
          name: user.name,
        });
      });
    }).catch(error => {
      console.log("Error: " + error.message);
    });

    return userData;
  }
  return null;
}

/*
 * get all user
 */
const getAllUsers = async(params = {userId: null, navigation: null}) => {
  if (params.userId) { // if user is logged in
    const userRef = firebase.firestore().collection('users').where('is_active', '==', true).where('is_approved', '==', true).where('profile_preference', '==', 'public').orderBy('first_name', 'asc');

    var usersData = [];

    await userRef.get().then(snapshot => {
      snapshot.forEach((res) => {
        if (res.id) {
          const user = res.data();

          if (user.is_active && user.is_approved && user.first_name) {
            usersData.push({
              id: res.id,
              name: capitalize(user.first_name) + ' ' + capitalize(user.last_name),
              first_name: capitalize(user.first_name),
              last_name: capitalize(user.last_name),
              profpic: user.profile_picture ? user.profile_picture : null,
              biography: user.biography,
              contact: user.contact_number,
              email: user.email,
              location: user.location,
              occupation: user.occupation,
              graduation: user.year_of_graduation,
              visibility: user.profile_preference,
              facebook: user.facebook_url,
              linkedin: user.linkedin,
              instagram: user.instagram,
              sponsorhip: user.sponsorship,
              sponsor_order: user.sponsor_order,
              last_seen: user.last_seen,
            });
          }
        } else {
          console.log("Document does not exist!");
        }
      });

    }).catch(error => {
      console.log("Error: " + error.message);
    });

    return usersData;
  }
  return null;
}

/*
 * get a user
 */
const getCompany = async(params = {comId: null, self: false}) => {
  var userData = {};

  if (params.self) { // if self
    const json_user_data = await AsyncStorage.getItem('logged_in_user');
    userData = json_user_data ? JSON.parse(json_user_data) : null;

  } else if (params.comId) { // to get a specific user
    const comRef = firebase.firestore().collection('businesses').doc(params.comId);
    const comSnapshot = await comRef.get();

    await comRef.get().then((res) => {
      if (res.id) {
        const user = res.data();

        if (user.name) {
          userData = {
            id: res.id,
            name: capitalize(user.name),
            profpic: user.profile_picture ? user.profile_picture : null,
            biography: user.biography,
            contact: user.contact_number,
            email: user.email,
            location: user.location,
            occupation: user.occupation,
            year: user.established_year.toString(),
            visibility: user.profile_preference,
            sponsor: user.is_sponsor,
            active: user.is_active,
            approved: user.is_approved,
            facebook: user.facebook_url,
            linkedin: user.linkedin,
            instagram: user.instagram,
          };
        }
      } else {
        console.log("Document does not exist!");
      }
    }).catch(error => {
      console.log("Error: " + error.message);
    });

    userData.profpic = userData.profpic ? await fburl(userData.profpic) : null;
  }

  return userData;
}

/*
 * get all companies
 */
const getAllCompanies = async(params = {userId: null, navigation: null}) => {
  if (params.userId) { // if user is logged in
    const comRef = firebase.firestore().collection('businesses').where('is_active', '==', true).where('is_approved', '==', true).where('business_profile_preference', '==', 'public').orderBy('name', 'asc');

    var comsData = [];

    await comRef.get().then(snapshot => {
      snapshot.forEach((res) => {
        if (res.id) {
          const com = res.data();

          if (com.is_active && com.is_approved && com.name) {
            comsData.push({
              id: res.id,
              name: capitalize(com.name),
              profpic: com.profile_picture ? com.profile_picture : null,
              biography: com.biography,
              contact: com.contact_number,
              email: com.email,
              location: com.location,
              occupation: com.occupation,
              year: com.established_year,
              visibility: com.business_profile_preference,
              sponsor: com.is_sponsor,
              facebook: com.facebook_url,
              linkedin: com.linkedin,
              instagram: com.instagram,
              sponsorship: com.sponsorship,
              sponsor_order: com.sponsor_order,
            });
          }
        } else {
          console.log("Document does not exist!");
        }
      });

    }).catch(error => {
      console.log("Error: " + error.message);
    });

    return comsData;
  }
  return null;
}

/*
 * get a user's jobs
 */
const getUserJobs = async (params = {userId: null}) => {
  if (params.userId) { // if user is logged in
    const userRef = firebase.firestore().collection('users').doc(params.userId);
    const comRef = firebase.firestore().collection('businesses');
    const comSnapshot = await comRef.get();

    var jobs = [];

    // get jobs
    await userRef.collection('jobs').get().then(jobSnapshot => {
      jobSnapshot.forEach(res => {
        const job = res.data();
        var company = "";

        // get company name
        if (job.company) {
          company = job.company;
        } else if (job.business_id) {
          comSnapshot.forEach((com) => {
            var comData = com.data();
            if ('businesses/' + com.id == job.business_id) {
              company = comData.name;
              return false;
            }
          });
        }
        jobs.push({
          id: res.id,
          name: capitalize(job.name),
          startDate: job.start_date ? job.start_date : job.start_year,
          endDate: job.end_date ? job.end_date : (job.end_year ? job.end_year : 'Current'),
          company: capitalize(company),
          companyName: capitalize(company),
        });
      });
    }).catch(error => {
      console.log("Error: " + error.message);
    });

    return jobs;
  }
  return null;
}

/*
 * get a list of promos
 */
const getPromoList = async (params = {userId: null, all: false, navigation: null, callback: null}) => {
  if (params.userId) { // if user is logged in
    const promoRef = !params.all ? firebase.firestore().collection('promotions').where('user_owner_id', '==', 'users/' + params.userId).orderBy('sequence', 'asc') : firebase.firestore().collection('promotions').where('is_approved', '==', true).where('is_active', '==', true).orderBy('sequence', 'asc');
    const imageRef = firebase.firestore().collection('images');
    // promoRef.where('is_active', '==', true).where('is_approved', '==', true).orderBy('sequence', 'asc').get().then((promoSnapshot);
    const imgSnapshot = await imageRef.get();

    var promoData = [];

    await promoRef.get().then(promoSnapshot => {

      promoSnapshot.forEach(res => {
        const promo = res.data();

        if (params.all || promo.user_owner_id == 'users/' + params.userId || promo.user_owner_id == '/users/' + params.userId) {
          var img_url = '';
          const imgref = 'promotions/'+res.id;

          // get the correct image
          imgSnapshot.forEach((res2) => {
            const res2data = res2.data();
            if (res2data.ref_id == imgref) {
              img_url = res2data.image;
              return false;
            }
          });

          // set promotions data
          promoData.push({
            id: res.id,
            name: promo.name,
            description: promo.description,
            image: img_url ? img_url : require('@img/empty.png'),
            approved: promo.is_approved,
            url: function() { params.navigation ? (params.all ? params.navigation.navigate('PromotionNavigation', {screen: 'ViewPromotion', params:{id: res.id}}) : params.navigation.navigate('MyEPNavigation', {screen: 'EditPromotion', params: params.callback ? {id: res.id, callback: params.callback} : {id: res.id}}) ) : null }
          });
        }
      });
    }).catch(error => {
      console.log("Error: " + error.message);
    });

    return promoData;
  }
  return null;
}

/*
 * get a list of communities
 */
const getCommunityList = async (params: {userId: null, all: false, navigation: null, callback: null}) => {
  if (params.userId) { // if user is logged in
    const communityRef = !params.all ? firebase.firestore().collection('communities').where('user_owner_id', '==', 'users/' + params.userId).orderBy('sequence', 'asc') : firebase.firestore().collection('communities').where('is_approved', '==', true).where('is_active', '==', true).orderBy('sequence', 'asc');
    const imageRef = firebase.firestore().collection('images');
    // communityRef.where('is_active', '==', true).where('is_approved', '==', true).orderBy('sequence', 'asc').get().then((communitySnapshot);
    const imgSnapshot = await imageRef.get();

    var communityData = [];

    await communityRef.get().then(communitySnapshot => {

      communitySnapshot.forEach((res) => {
        const community = res.data();

        if (params.all || community.user_owner_id == 'users/' + params.userId || community.user_owner_id == '/users/' + params.userId) {
          var img_url = '';
          const imgref = 'communities/'+res.id;

          // get the correct image
          imgSnapshot.forEach((res2) => {
            const res2data = res2.data();
            if (res2data.ref_id == imgref) {
              img_url = res2data.image;
              return false;
            }
          });

          // set communities data
          communityData.push({
            id: res.id,
            name: community.name,
            description: community.description,
            image: img_url ? img_url : require('@img/empty.png'),
            approved: community.is_approved,
            url: function() { params.navigation ? (params.all ? params.navigation.navigate('CommunityNavigation', {screen: 'ViewCommunity', params:{id: res.id}}) : params.navigation.navigate('MyEPNavigation', {screen: 'EditCommunity', params: params.callback ? {id: res.id, callback: params.callback} : {id: res.id}}) ) : null }
          });
        }
      });
    }).catch(error => {
      console.log("Error: " + error.message);
    });

    return communityData;
  }
  return null;
}

/*
 * get a list of posters
 */
const getPosterList = async (params = {navigation: null}) => {
  const promoRef = firebase.firestore().collection('promotions').where('is_approved', '==', true).where('is_active', '==', true).where('is_featured', '==', true).orderBy('sequence', 'asc');
  //const communityRef = firebase.firestore().collection('communities').where('is_approved', '==', true).where('is_active', '==', true).where('is_featured', '==', true).orderBy('sequence', 'asc');
  const imageRef = firebase.firestore().collection('images');
  const imgSnapshot = await imageRef.get();
  var promoData = [], communityData = [], posterData = [];

  await promoRef.get().then(promoSnapshot => {

    promoSnapshot.forEach((res) => {
      const promo = res.data();
      const imgref = 'promotions/'+res.id;
      var img_url = '';

      // get the correct image
      imgSnapshot.forEach((res2) => {
        const res2data = res2.data();
        if (res2data.ref_id == imgref) {
          img_url = res2data.image;
          return false;
        }
      });

      if (img_url) {
        // set promos data
        promoData.push({
          id: res.id,
          image: img_url,
          name: promo.name,
          sequence: promo.sequence,
          url: function() { params.navigation ? params.navigation.navigate('Login', {promo: res.id}) : null }
        });
      } else {
        console.log("Error: Need an image.");
      }

    });
  }).catch(error => {
    console.log("Error: " + error.message);
  });

  // await communityRef.get().then(communitySnapshot => {
  //
  //   communitySnapshot.forEach((res) => {
  //     const community = res.data();
  //     const imgref = 'communities/'+res.id;
  //     var img_url = '';
  //
  //     // get the correct image
  //     imgSnapshot.forEach((res2) => {
  //       const res2data = res2.data();
  //       if (res2data.ref_id == imgref) {
  //         img_url = res2data.image;
  //         return false;
  //       }
  //     });
  //
  //     // set communities data
  //     communityData.push({
  //       image: img_url ? img_url : require('@img/empty.png'),
  //       sequence: community.sequence,
  //       url: function() { params.navigation ? params.navigation.navigate('Login', {params:{community: res.id}}) : null }
  //     });
  //   });
  // }).catch(error => {
  //   console.log("Error: " + error.message);
  // });

  posterData = promoData.concat(communityData);

  function compare( a, b ) {
    if ( a.sequence < b.sequence ){
      return -1;
    }
    if ( a.sequence > b.sequence ){
      return 1;
    }
    return 0;
  }

  posterData.sort(compare);

  return posterData;
}

/*
 * get a promo data
 */
const getPromo = async (params = {userId: null, promoId: null}) => {
  if (params.promoId) {
    const promoRef = firebase.firestore().collection('promotions').doc(params.promoId);
    const imageRef = firebase.firestore().collection('images');

    const imgSnapshot = await imageRef.get();

    var promoData = {};

    await promoRef.get().then((res) => {
      if (res.id) {
        var img_url = '';
        const imgref = 'promotions/'+res.id;

        // get the correct image
        imgSnapshot.forEach((res2) => {
          const res2data = res2.data();
          if (res2data.ref_id == imgref) {
            img_url = res2data.image;
            return false;
          }
        });

        const promo = res.data();
        promoData = {
          id: res.id,
          name: promo.name,
          image: img_url ? {uri: img_url} : null,
          details: promo.description,
          contact: promo.contact,
          startDate: promo.start_time,
          endDate: promo.end_time,
          location: promo.location,
          url: promo.promotion_url,
          location: promo.location,
          start_time: promo.start_time,
          end_time: promo.end_time,
        };
      } else {
        console.log("Document does not exist!");
      }
    }).catch(error => {
      console.log("Error: " + error.message);
    });

    return promoData;
  }
  return null;
}

/*
 * get an community data
 */
const getCommunity = async (params: {userId: null, communityId: null}) => {
  if (params.communityId) { // if user is logged in
    const communityRef = firebase.firestore().collection('communities').doc(params.communityId);
    const imageRef = firebase.firestore().collection('images');

    const imgSnapshot = await imageRef.get();

    var communityData = {};

    await communityRef.get().then(res => {
      if (res.id) {
        var img_url = '';
        const imgref = 'communities/'+res.id;

        // get the correct image
        imgSnapshot.forEach((res2) => {
          const res2data = res2.data();
          if (res2data.ref_id == imgref) {
            img_url = res2data.image;
            return false;
          }
        });

        const community = res.data();
        communityData = {
          id: res.id,
          name: community.name,
          image: img_url ? {uri: img_url} : null,
          details: community.description,
          contact: community.contact,
          startDate: community.start_time,
          endDate: community.end_time,
          location: community.location,
          url: community.registration_url,
          location: community.location,
          start_time: community.start_time,
          end_time: community.end_time,
        };
      } else {
        console.log("Document does not exist!");
      }
    }).catch(error => {
      console.log("Error: " + error.message);
    });

    return communityData;
  }
  return null;
}

/*
 * get an image data
 */
const getImage = async (params = {userId: null, refId: null}) => {
  if (params.refId) { // if user is logged in
    const imageRef = firebase.firestore().collection('images').where('ref_id', '==', params.refId);

    var ret = {};

    await imageRef.get().then(snapshot => {
      snapshot.forEach((res) => {
        ret = res.data();
        ret.id = res.id;
        return false;
      });
    }).catch(error => {
      console.log('Error: ' + error);
    });

    return ret;
  }
  return null;
}

const fburl = async (url) => {
  var ref = null;
  if (typeof url == 'string') {
    url = url.indexOf('/') > -1 ? url.substring(url.lastIndexOf('/') + 1) : url;
    ref = await storage().ref(url).getDownloadURL().catch(error => {
      console.log(error);
    });
  }
  return ref;
}

/*
 * get a list sponsor
 */
const getSponsorList = async (params = {userId: null, navigation: null}) => {
  if (params.userId) { // if user is logged in
    const userRef = firebase.firestore().collection('users').where('is_active', '==', true).where('is_approved', '==', true).where('profile_preference', '==', 'public').orderBy('sponsor_order', 'asc');
    const businessRef = firebase.firestore().collection('businesses').where('is_active', '==', true).where('is_approved', '==', true).where('profile_preference', '==', 'public').orderBy('sponsor_order', 'asc');

    var userData = [], businessData = [], sponsorData = [];

    await userRef.get().then(userSnapshot => {

      userSnapshot.forEach((res) => {
        // set user data
        const user = res.data();

        if (user.sponsorship != '') {
          userData.push({
            id: res.id,
            name: user.first_name + ' ' + user.last_name,
            first_name: user.first_name,
            last_name: user.last_name,
            image: user.profile_picture ? user.profile_picture : require('@img/empty_user.png'),
            url: function() { params.navigation ? params.navigation.navigate('ProfileScreen', {userId: res.id}) : null }
          });
        }
      });
    }).catch(error => {
      console.log("Error: " + error.message);
    });

    for (const user of userData) {
      const img = await fburl(user.image);
      user.image = img ? img : user.image;
    }

    await businessRef.get().then(businessSnapshot => {

      businessSnapshot.forEach((res) => {
        // set communities data
        const business = res.data();
        if (business.sponsorship != '') {
          const a = async () => {
            const img = await fburl(business.profile_picture);
            businessData.push({
              name: business.name,
              image: img ? img : require('@img/empty_user.png'),
              url: function() { params.navigation ? params.navigation.navigate('ProfileScreen', {userId: res.id}) : null }
            });
          }
          a();
        }
      });
    }).catch(error => {
      console.log("Error: " + error.message);
    });

    for (const business of businessData) {
      const img = await fburl(business.image);
      business.image = img ? img : business.image;
    }

    sponsorData = userData.concat(businessData);

    function compare(a, b) {
      if (a.sequence < b.sequence) return -1;
      if (a.sequence > b.sequence) return 1;
      return 0;
    }

    sponsorData.sort(compare);

    return sponsorData;
  }
  return null;
}

const Functions = {
  addUser: addUser,
  addBusiness: addBusiness,
  capitalize: capitalize,
  checkUserExists: checkUserExists,
  fburl: fburl,
  getUser: getUser,
  getCompany: getCompany,
  getAllUsers: getAllUsers,
  getAllCompanies: getAllCompanies,
  getCommunity: getCommunity,
  getCommunityList: getCommunityList,
  getImage: getImage,
  getPosters: getPosterList,
  getPromo: getPromo,
  getPromoList: getPromoList,
  getSponsorList: getSponsorList,
}

export default Functions;

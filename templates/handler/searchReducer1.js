import { combineReducers } from 'redux';
import { label } from '@handler/data';

const data = {
  search: {type: 'All', name: '', job: '' },
  types: [{label: label.all, value: label.all}, {label: label.people, value: label.people}, {label: label.company, value: label.company}],
};

const searchReducer = (state = data, action) => {
  const { search, types } = state;
  switch (action.type) {
    case 'SEARCH_TYPE':
      search.type = action.payload;
      break;
    case 'SEARCH_NAME':
      search.name = action.payload;
      break;
    case 'SEARCH_JOB':
      search.job = action.payload;
      break;
    default:
      break;
  }
  return { search, types };
};

export default combineReducers({
  searchRed: searchReducer
});

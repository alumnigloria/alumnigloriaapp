// init actions
export const setState = payload => ({ type: 'STATE', payload: payload });
export const setStateInit = payload => ({ type: 'STATE_INIT', payload: payload });
export const setStateUser = payload => ({ type: 'STATE_USER', payload: payload });
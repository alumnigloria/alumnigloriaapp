// searching actions
export const resetSearchData = payload => ({ type: 'SEARCH_RESET' });
export const searchAct = payloads => ({ type: 'SEARCH', payload: payloads, });
export const setSearchType = payload => ({ type: 'SEARCH_TYPE', payload: payload });
export const setSearchName = payload => ({ type: 'SEARCH_NAME', payload: payload });
export const setSearchJob = payload => ({ type: 'SEARCH_JOB', payload: payload });

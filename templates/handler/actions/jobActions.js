// profile actions
export const resetjobData = payload => ({ type: 'JOB_RESET' });
export const setJob = payload => ({ type: 'JOB', payload: payload });
export const setJobName = payload => ({ type: 'JOB_NAME', payload: payload });
export const setJobCompany = payload => ({ type: 'JOB_COMPANY', payload: payload });
export const setJobStartYear = payload => ({ type: 'JOB_START_YEAR', payload: payload });
export const setJobEndYear = payload => ({ type: 'JOB_END_YEAR', payload: payload });
// export const setJobBusiness = payload => ({ type: 'JOB_BUSINESS', payload: payload });
// export const setJobStartDate = payload => ({ type: 'JOB_START_DATE', payload: payload });
// export const setJobEndDate = payload => ({ type: 'JOB_END_DATE', payload: payload });

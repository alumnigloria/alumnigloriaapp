// profile actions
export const resetProfileData = payload => ({ type: 'PROFILE_RESET' });
export const setProfile = payload => ({ type: 'PROFILE', payload: payload });
export const setProfileName = payload => ({ type: 'PROFILE_NAME', payload: payload });
export const setProfileFirstName = payload => ({ type: 'PROFILE_FIRSTNAME', payload: payload });
export const setProfileLastName = payload => ({ type: 'PROFILE_LASTNAME', payload: payload });
export const setProfileImage = payload => ({ type: 'PROFILE_IMAGE', payload: payload });
export const setProfileOccupation = payload => ({ type: 'PROFILE_OCCUPATION', payload: payload });
export const setProfileGrad = payload => ({ type: 'PROFILE_GRADUATION', payload: payload });
export const setProfileYear = payload => ({ type: 'PROFILE_YEAR', payload: payload });
export const setProfileEmail = payload => ({ type: 'PROFILE_EMAIL', payload: payload });
export const setProfileTelp = payload => ({ type: 'PROFILE_TELP', payload: payload });
export const setProfileAddress = payload => ({ type: 'PROFILE_ADDRESS', payload: payload });
export const setProfileFacebook = payload => ({ type: 'PROFILE_FACEBOOK', payload: payload });
export const setProfileInstagram = payload => ({ type: 'PROFILE_INSTAGRAM', payload: payload });
export const setProfileLinkedin = payload => ({ type: 'PROFILE_LINKEDIN', payload: payload });
export const setProfileJobs = payload => ({ type: 'PROFILE_JOBS', payload: payload });

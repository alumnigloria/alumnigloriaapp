// community actions
export const resetCommunityData = payload => ({ type: 'COMMUNITY_RESET' });
export const setCommunity = payload => ({ type: 'COMMUNITY', payload: payload });
export const setCommunityName = payload => ({ type: 'COMMUNITY_NAME', payload: payload });
export const setCommunityImage = payload => ({ type: 'COMMUNITY_IMAGE', payload: payload });
export const setCommunityContact = payload => ({ type: 'COMMUNITY_CONTACT', payload: payload });
export const setCommunityUrl = payload => ({ type: 'COMMUNITY_URL', payload: payload });
export const setCommunityLocation = payload => ({ type: 'COMMUNITY_LOCATION', payload: payload });
export const setCommunityStartTime = payload => ({ type: 'COMMUNITY_START_TIME', payload: payload });
export const setCommunityEndTime = payload => ({ type: 'COMMUNITY_END_TIME', payload: payload });
export const setCommunityDetails = payload => ({ type: 'COMMUNITY_DETAILS', payload: payload });

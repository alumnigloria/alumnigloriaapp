// profile actions
export const resetUserData = payload => ({ type: 'USER_RESET' });
export const setUser = payload => ({ type: 'USER', payload: payload });
export const setUserEmail = payload => ({ type: 'USER_EMAIL', payload: payload });
export const setUserPassword = payload => ({ type: 'USER_PASSWORD', payload: payload });
export const setUserName = payload => ({ type: 'USER_NAME', payload: payload });
export const setUserFirstName = payload => ({ type: 'USER_FIRST_NAME', payload: payload });
export const setUserLastName = payload => ({ type: 'USER_LAST_NAME', payload: payload });
export const setUserGrad = payload => ({ type: 'USER_GRADUATION', payload: payload });
export const setUserYear = payload => ({ type: 'USER_YEAR', payload: payload });
export const setUserLocation = payload => ({ type: 'USER_LOCATION', payload: payload });
export const setUserTelp = payload => ({ type: 'USER_TELP', payload: payload });

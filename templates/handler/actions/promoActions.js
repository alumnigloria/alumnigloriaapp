// promo actions
export const resetPromoData = payload => ({ type: 'PROMO_RESET' });
export const setPromo = payload => ({ type: 'PROMO', payload: payload });
export const setPromoName = payload => ({ type: 'PROMO_NAME', payload: payload });
export const setPromoImage = payload => ({ type: 'PROMO_IMAGE', payload: payload });
export const setPromoContact = payload => ({ type: 'PROMO_CONTACT', payload: payload });
export const setPromoUrl = payload => ({ type: 'PROMO_URL', payload: payload });
export const setPromoLocation = payload => ({ type: 'PROMO_LOCATION', payload: payload });
export const setPromoStartTime = payload => ({ type: 'PROMO_START_TIME', payload: payload });
export const setPromoEndTime = payload => ({ type: 'PROMO_END_TIME', payload: payload });
export const setPromoDetails = payload => ({ type: 'PROMO_DETAILS', payload: payload });

import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, TextInput, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements'; // styling see at react-native-elements doc
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Layout from '@layout/basic';
import { LabelInput } from '@elements/input';
import { Color, Input, Size, dstyles, label } from '@handler/data';
import { img } from '@handler/img';
import { sha256 } from 'react-native-sha256';
import GlobalFunc from '@handler/functions';
import { resetUserData, setUser, setUserEmail, setUserPassword, setUserName, setUserYear, setUserLocation, setUserTelp } from '@actions/userActions';
import moment from 'moment';


function CompanyRegisterScreen(props) {
  const params = props.route.params;
  const navigation = props.navigation;
  const [msgTitle, setMsgTitle] = useState(false);
  const [msg, setMsg] = useState(false);
  const height = 700;
  const [loadingText, setLoadingText] = useState('');
  const [adding, setAdding] = useState(false);
  const [errors, setErrors] = useState([]);
  const [addResult, setAddResult] = useState(true);
  const [setup, setSetup] = useState(true);
  const editCallback = params && params.callback ? params.callback : null;

  // validate inputs
  const validateInputs = function() {
    var errs = [];
    if (props.cuserRed.cuser.name == '') {
      errs.push(label.errorMsgNameEmpty);
    }
    if (props.cuserRed.cuser.email != '' && !/\w+@\w+\.\w+/.test(props.cuserRed.cuser.email)) {
      errs.push(label.errorMsgEmailFormat);
    }
    if (props.cuserRed.cuser.password == '') {
      errs.push(label.errorMsgPasswordEmpty);
    } else if (props.cuserRed.cuser.password.length < 6 || !/(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}/g.test(props.cuserRed.cuser.password)) {
      errs.push(label.errorMsgPasswordReq);
    }
    if (props.cuserRed.cuser.year == '') {
      errs.push(label.errorMsgYearEmpty);
    } else if (!/\d+/.test(props.cuserRed.cuser.year)) {
      errs.push('Established Year year must be a number.');
    }
    if (props.cuserRed.cuser.location == '') {
      errs.push(label.errorMsgLocationEmpty);
    }
    return errs;
  }

  // register
  const companyRegistration = async () => {
    var errs = validateInputs();

    if (errs.length > 0) { // Error
      setMsgTitle(label.errorMsgCUserRegisterFailed);
      setMsg('\n'+errs.join('\n'));
      setLoadingText('');
      setErrors(errs);
      setAddResult(true);

    } else {
      var existingUser = await GlobalFunc.checkUserExists({email: props.cuserRed.cuser.email, type:'businesses'});
      if (existingUser === null || existingUser.length > 0) {
        errs.push(label.errorMsgCEmailExist);
      }

      // sha256(props.cuserRed.cuser.password).then( hash => {
      //   props.setUserPassword(hash);
      // });

      var docData = {
        is_active: true,
        is_approved: false,
        is_staff: false,
        is_super_user: false,
        email: props.cuserRed.cuser.email.toLowerCase(),
        password: props.cuserRed.cuser.password,
        name: props.cuserRed.cuser.name,
        established_year: props.cuserRed.cuser.year,
        location: props.cuserRed.cuser.location,
        contact_number: props.cuserRed.cuser.telp,
        biography: '',
        address: '',
        business_logo: '',
        business_profile_preference: 'public',
        business_url: '',
        description: '',
        instagram: '',
        linkedin: '',
        facebook_url: '',
        occupation: '',
        sponsorship: 'none',
        sponsor_order: 0,
        last_seen: moment(new Date()).format('DD MMM YYYY hh:mm'),
        last_updated_time: moment(new Date()).format('DD MMM YYYY hh:mm'),
        last_updated_uid: '',
        created_time: moment(new Date()).format('DD MMM YYYY hh:mm'),
        created_uid: '',
      };

      var msg = '', docId = null;

      // write
      [docId, msg] = await GlobalFunc.addBusiness(docData);

      setMsgTitle('');
      setMsg(docId ? "Company registration request sent, please wait until the registration approved." : msg);
      setLoadingText('');
      setErrors(docId ? false : true);

      if (typeof editCallback === 'function') {
        editCallback();
      }
      setAddResult(docId ? docId : true);
    }
    setAdding(false);
  }

  const callback = function() {
    setMsg(false);
    setLoadingText('');
    if (!errors) navigation.goBack();
  }

  const addPressed = function() {
    setLoadingText('Registering a new business...');
    setAdding(true);
  }

  useEffect(function() {
    if (setup) {
      setSetup(false);
      props.resetUserData();
      setAddResult(true);
    }
    if (adding && addResult) {
      setAddResult(false);
      setTimeout(function() { companyRegistration(); }, 1000);
      
    }
  }, [setup, adding]);

  return (
    adding || !addResult ? <View style={dstyles.container}>
      <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{loadingText}</Text></View>
    </View> :
    <Layout style={dstyles.container} height={height} title={msgTitle} msg={msg} callback={callback}>
      <View style={{display: 'flex'}}>
        <View style={[dstyles.tightContent, dstyles.bgTheme, {paddingTop: Size.xxl, paddingBottom: Size.xxl}]}>
          <Text style={[dstyles.title, dstyles.textWhite]}>{'New Company Registration'}</Text>
          <View style={dstyles.bgTheme}>
            <LabelInput title={label.email + "*"} placeholder={label.inputEmail} type={2} onChange={(e) => props.setUserEmail(e)} value={props.cuserRed.cuser.email} />
            <LabelInput title={label.password + "*"} placeholder={label.inputPassword} secureTextEntry={true} type={2} onChange={(e) => props.setUserPassword(e)} value={props.cuserRed.cuser.password} />
            <LabelInput title={label.name + "*"} placeholder={label.inputName} type={2} onChange={(e) => props.setUserName(e)} value={props.cuserRed.cuser.name} />
            <LabelInput title={label.establishedYear + "*"} placeholder={label.inputYear} type={2} onChange={(e) => props.setUserYear(e)} value={props.cuserRed.cuser.year} />
            <LabelInput title={label.location + "*"} placeholder={label.inputLocation} type={2} onChange={(e) => props.setUserLocation(e)} value={props.cuserRed.cuser.location} />
            <LabelInput title={label.telp} placeholder={label.inputTelp} type={2} onChange={(e) => props.setUserTelp(e)} value={props.cuserRed.cuser.telp} />
          </View>
        </View>
      </View>
      <View style={styles.textCont}>
        <View style={dstyles.alignMiddle}>
          <Button type="solid" title={label.register} buttonStyle={[dstyles.button, dstyles.bgSecondary]} onPress={addPressed} />
        </View>
        <View style={dstyles.alignMiddle}>
          <Button type="solid" title={label.back} buttonStyle={[dstyles.button, dstyles.bgSecondary]} onPress={() => navigation.goBack()} />
        </View>
      </View>
    </Layout>
  );
}

const styles = StyleSheet.create({
  logo: {
    flex:1,
    position: 'absolute',
    width: '100%',
    height: 90,
    top: 0,
    alignSelf: 'center',
    zIndex: 2,
  },
  logoCont: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 260,
  },
  back: {
    minWidth: "100%",
    height: 260,
    opacity: 0.1,
    zIndex: 1,
  },
  textCont: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'center',
    alignItems: 'center', // horizontal align
    justifyContent: 'center',
    marginTop: 0,
    width: 300,
    padding: Size.lg,
  },
  text: {
    fontSize: 14,
    color: '#ffffff'
  }
});

const mapStateToProps = (state) => {
  const { cuserRed } = state
  return { cuserRed }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    resetUserData,
    setUser,
    setUserEmail,
    setUserPassword,
    setUserName,
    setUserYear,
    setUserLocation,
    setUserTelp,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(CompanyRegisterScreen);

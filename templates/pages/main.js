import React from 'react';
import { SafeAreaView, ScrollView, StyleSheet, View } from 'react-native';
import MainDrawer from '@elements/mainDrawer';

export default function MainScreen(props) {
  return (
    <MainDrawer {...props} />
  );
}

import React, { useState, useEffect } from 'react';
import { Linking, RefreshControl, ScrollView, StyleSheet, View } from 'react-native';
import { Button, Text } from 'react-native-elements';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { CommonActions, StackActions } from '@react-navigation/native';
import BorderedImage from '@elements/image';
import CustomHeader from '@elements/header';
import { Color, Size, dstyles, label } from '@handler/data';
import GlobalFunc from '@handler/functions';
import { icons, img } from '@handler/img';

var data, occupation, graduation, email, telp, address, buttons, year, fb, insta, linkedin, jobs;

//const userDocument = firestore().collection('Users').doc('ABC');

function setupProfileData(data) {
  occupation = data.occupation ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.occupation}</Text> : null;
  graduation = data.graduation ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.graduation}</Text> : null;
  email = data.email ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.email}</Text> : null;
  telp = data.telp ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.contact_number}</Text> : null;
  address = data.address ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.address}</Text> : null;
  fb = data.facebook ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.facebook}</Text> : null;
  insta = data.instagram ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.instagram}</Text> : null;
  linkedin = data.linkedin ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.linkedin}</Text> : null;
  jobs = data.jobs && data.jobs.length ? <View>{
    data.jobs.map((job, i) => (
      <View style={dstyles.text} key={i}><Text style={[dstyles.textWhite, dstyles.textCenter]}>{job.startDate + ' - ' + job.endDate}</Text><Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter, dstyles.textBold]}>{job.name + ' in ' + job.companyName}</Text></View>
    )) }</View> : <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{label.noJob}</Text>;
}

function setupCompanyProfileData(data) {
  occupation = data.occupation ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.occupation}</Text> : null;
  year = data.year ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.year}</Text> : null;
  email = data.email ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.email}</Text> : null;
  telp = data.telp ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.telp}</Text> : null;
  address = data.address ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.address}</Text> : null;
  fb = data.facebook ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.facebook}</Text> : null;
  insta = data.instagram ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.instagram}</Text> : null;
  linkedin = data.linkedin ? <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter]}>{data.linkedin}</Text> : null;
}

export function ViewProfileScreen(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [data, setData] = useState({});
  const [user_id, setUser] = useState([]);
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);

  const refresh = () => {
    setLoading(true);
  }

  if (params.userId) {
    var x = {a: params.userId};
    params.userId = null;
    setUser(x.a);
    refresh();
  }
  //if (params && params.userId == undefined) navigation.goBack();

  if (loading) {
    async function setUserData() {
      let userData = await GlobalFunc.getUser({self: true});
      var selfData = {};

      if (userData) { // if user is logged in
        selfData = await GlobalFunc.getUser({userId: user_id});
      }

      setLoading(false);
      setRefreshing(false);
      setupProfileData(selfData);
      setData(selfData);
    }
    setUserData();
  } else {
    setupProfileData(data);
  }

  if (navigation.state && navigation.state.routeName != 'ViewProfile')
    navigation.dispatch(
      StackActions.replace('ViewProfile')
    );

  const callUser = function() {
    if (data.contact_number)
      Linking.openURL(`tel:${data.contact_number}`);
  }
  const emailUser = function() {
    Linking.openURL('mailto:'+data.email);
  }
  const back = function() {
    navigation.goBack();
  }

  buttons = loading ? null : <View style={[dstyles.content, dstyles.buttons, {marginBottom: Size.lg}]}><View style={[dstyles.row, dstyles.alignMiddle]}>{data.contact_number ? <Button type="solid" title={label.call} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={callUser} /> : null}<Button type="solid" title={label.email} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={emailUser} /></View><View style={[dstyles.row, dstyles.alignMiddle]}><Button type="solid" title={label.back} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={back} /></View></View>;

  return (
    <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.app} />
      {
        loading ? <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{label.loading}</Text></View> :
        <ScrollView style={dstyles.scrollViewContent} refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={refresh} />
        }>
          <View style={dstyles.bgTheme}>
            <View style={[dstyles.tightContent, {paddingTop: Size.xxl, paddingBottom: 50}]}>
              <BorderedImage source={{uri: data.profpic}} title={label.image} circle={true} />
              <Text style={[dstyles.pageTitle, dstyles.textWhite]}>{data.name}</Text>
              { occupation }
              { graduation }
              { email }
              { telp }
              { address }
              { fb }
              { insta }
              { linkedin }
              <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter, dstyles.textBold, {marginTop: Size.lg}]}>{label.jobs+':'}</Text>
              { jobs }
            </View>
          </View>
          { buttons }
        </ScrollView>
      }
    </View>
  );
}

export function ViewOwnProfileScreen(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);

  const refresh = () => {
    setLoading(true);
  }

  if (loading) {
    async function setUserData() {
      let userData = await GlobalFunc.getUser({self: true});
      var selfData = {};

      if (userData) { // if user is logged in
        selfData = await GlobalFunc.getUser({userId: userData.id});
      }

      setLoading(false);
      setRefreshing(false);
      setupProfileData(selfData);
      setData(selfData);
    }
    setUserData();
  } else {
    setupProfileData(data);
  }

  if (navigation.state && navigation.state.routeName != 'ViewOwnProfile')
    navigation.dispatch(
      StackActions.replace('ViewOwnProfile')
    );

  const editUser = function() {
    navigation.navigate('ProfileNavigation', {screen: 'EditProfile', params: {callback: refresh}});
  }

  buttons = loading ? null : <View style={[dstyles.content, dstyles.buttons, {marginBottom: Size.lg}]}><View style={[dstyles.row, dstyles.alignMiddle]}><Button type="solid" title={label.edit} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={editUser} /></View></View>;

  return (
    <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.app} />
      {
        loading ? <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{label.loading}</Text></View> :
        <ScrollView style={dstyles.scrollViewContent} refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={refresh} />
        }>
          <View style={dstyles.bgTheme}>
            <View style={[dstyles.tightContent, {paddingTop: Size.xxl, paddingBottom: 50}]}>
              <BorderedImage source={data.profpic} title={label.image} circle={true} />
              <Text style={[dstyles.pageTitle, dstyles.textWhite]}>{data.name}</Text>
              { occupation }
              { graduation }
              { email }
              { telp }
              { address }
              { fb }
              { insta }
              { linkedin }
              <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter, dstyles.textBold, {marginTop: Size.lg}]}>{label.jobs+':'}</Text>
              { jobs }
            </View>
          </View>
          { buttons }
        </ScrollView>
      }
    </View>
  );
}

export function ViewCompanyProfileScreen(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [data, setData] = useState({});
  const [com_id, setCom] = useState([]);
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);

  const refresh = () => {
    setLoading(true);
  }

  if (params.comId) {
    var x = {a: params.comId};
    params.comId = null;
    setCom(x.a);
    refresh();
  }

  if (loading) {
    async function setUserData() {
      let userData = await GlobalFunc.getUser({self: true});
      var selfData = {};

      if (userData) { // if user is logged in
        selfData = await GlobalFunc.getCompany({comId: com_id});
      }

      setLoading(false);
      setRefreshing(false);
      setupProfileData(selfData);
      setData(selfData);
    }
    setUserData();
  } else {
    setupCompanyProfileData(data);
  }

  if (navigation.state && navigation.state.routeName != 'ViewCompanyProfile')
    navigation.dispatch(
      StackActions.replace('ViewCompanyProfile')
    );

  const callCompany = function() {
    Linking.openURL(`tel:${data.contact_number}`);
  }

  const emailCompany = function() {
    Linking.openURL('mailto:'+data.email);
  }

  const editCompany = function() {
    navigation.navigate('ProfileNavigation', {screen: 'EditProfile'});
  }

  buttons = loading ? null : <View style={[dstyles.content, dstyles.buttons, {marginBottom: Size.lg}]}><View style={[dstyles.row, dstyles.alignMiddle]}>{data.contact_number ? <Button type="solid" title={label.call} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={callCompany} /> : null}<Button type="solid" title={label.email} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={emailCompany} /></View><View style={[dstyles.row, dstyles.alignMiddle]}><Button type="solid" title={label.back} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={() => navigation.goBack()} /></View></View>;
  // const buttons = params && params.self ?
  //   <View style={[dstyles.row, dstyles.alignMiddle]}><Button type="solid" title={label.edit} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={editCompany} /></View> :
  //   <View style={[dstyles.row, dstyles.alignMiddle]}><Button type="solid" title={label.call} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={callCompany} /><Button type="solid" title={label.email} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={emailCompany} /></View>;

  return (
    <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.app} />
      {
        loading ? <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{label.loading}</Text></View> :
        <ScrollView style={dstyles.scrollViewContent} refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={refresh} />
        }>
          <View style={dstyles.bgTheme}>
            <View style={[dstyles.tightContent, {paddingTop: Size.xxl, paddingBottom: 50}]}>
              <BorderedImage source={data.profpic} title={label.image} circle={true} />
              <Text style={[dstyles.pageTitle, dstyles.textWhite]}>{data.name}</Text>
              { occupation }
              { year }
              { email }
              { telp }
              { address }
              { fb }
              { insta }
              { linkedin }
            </View>
          </View>
          { buttons }
        </ScrollView>
      }
    </View>
  );
}

const styles = StyleSheet.create({
});

import React, { useState } from 'react';
import { ScrollView, StyleSheet, TextInput, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { icons, img } from '@handler/img';
import Banner from '@elements/banner';
import CustomHeader from '@elements/header';
import { LabelInput, PickerInput } from '@elements/input';
import { Color, Size, dstyles, label } from '@handler/data';
import { searchAct, setSearchType, setSearchName, setSearchJob } from '@actions/searchActions';


function SearchDetailsScreen(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;

  const search = function() {
    if (params.callback != undefined) params.callback();
    navigation.goBack();
  }

  // <LabelInput title={label.name} value={props.searchRed.search.name} placeholder={label.input} type={2} inline={true} onChange={(e) => props.setSearchName(e)} />
  // <LabelInput title={label.occupation} value={props.searchRed.search.job} placeholder={label.input} type={2} inline={true} onChange={(e) => props.setSearchJob(e)} />

  return (
    <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.search} options={{back: true, menu: false}} />
      <ScrollView style={[dstyles.scrollViewContent, dstyles.bgTheme]}>
        <View style={dstyles.bgTheme}>
          <View style={dstyles.tightContent}>
            <Text style={[dstyles.title, dstyles.textWhite]}>{label.search_custom}</Text>
            <PickerInput title={label.type} values={props.searchRed.types} type={2} inline={true} prop={'search_type'} onChange={(v,i) => props.setSearchType(v)} selectedValue={props.searchRed.search.type} />

          </View>
          <View style={[dstyles.alignMiddle, {marginBottom: Size.lg}]}>
            <Button type="solid" title={label.search} buttonStyle={[dstyles.button2, dstyles.bgThemeDark]} onPress={search} />
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  selectArrowCont: {
    position: 'absolute',
    top:66,
    right: 45,
    zIndex: 3,
  },
  selectArrow: {
    width: 12,
    height: 8,
  }
});

const mapStateToProps = (state) => {
  const { searchRed } = state
  return { searchRed }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    searchAct,
    setSearchType,
    setSearchName,
    setSearchJob,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(SearchDetailsScreen);

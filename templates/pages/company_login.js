import React, { useState } from 'react';
import { ScrollView, StyleSheet, TextInput, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements';
import { StackActions } from '@react-navigation/native';
import Layout from '@layout/basic';
import { Color, Input, Size, dstyles, label } from '@handler/data';
import { img } from '@handler/img';
import { sha256 } from 'react-native-sha256';
import firebase from "../../config/firebase";
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-community/async-storage';


export default function CompanyLoginScreen(props) {
  const params = props.route.params;
  const navigation = props.navigation;
  const [u, setU] = useState('');
  const [p, setP] = useState('');
  const [logging, setLogging] = useState(false);
  const [msg, setMsg] = useState(false);
  const height = 550;

  const getCircularReplacer = () => {
    const seen = new WeakSet();
    return (key, value) => {
    if (typeof value === "object" && value !== null) {
        if (seen.has(value)) {
            return;
        }
        seen.add(value);
    }
    return value;
    };
  };

  async function login() {
    if (u != '' && p != '' && !logging) {
      setLogging(true);
      // sha256(p).then( hash => { setP(hash); }); // don't save password in db, use firebase auth instead
      var found_user = false;
      var business_data = {};
      const userRef = await firebase.firestore().collection('businesses');
      userRef.where('email', '==', u.toLowerCase()).get().then(async (querySnapshot) => {
        querySnapshot.forEach((res) => {
          business_data = res.data();
          business_data['id'] = res.id;
          business_data['user_type'] = 'business';
          delete business_data['password'];
        });
        if (Object.keys(business_data).length > 0) {
          if (business_data.is_active && business_data.is_approved) {
            // Firebase auth
            await auth()
              .signInWithEmailAndPassword(u, p)
              .then(async () => {
                console.log('Business account created & signed in!');
                const saveSession = async () => {
                  try {
                    await AsyncStorage.setItem('logged_in_user', JSON.stringify(business_data, getCircularReplacer()));
                    found_user = true;
                    console.log('Login Success');
                    navigation.dispatch(StackActions.pop(1));
                    if (params && params.promo)
                      navigation.dispatch(StackActions.replace('Main', {screen: 'PromotionNavigation', params: {id: params.promo}}));
                    else if (params && params.community)
                      navigation.dispatch(StackActions.replace('Main', {screen: 'CommunityNavigation', params: {id: params.promo}}));
                    else navigation.dispatch(StackActions.replace('Main'));
                  } catch (error) {
                    console.log("Error Save Session: " + error.message);
                    setMsg(label.errorMsgLoginUPWrong);
                  }
                };
                await saveSession();
              })
              .catch(error => {
                if (error.code === 'auth/invalid-email') {
                  setMsg(label.errorMsgEmailInvalid);
                } else if (error.code === 'auth/wrong-password') {
                  setMsg(label.errorMsgPasswordInvalid);
                } else if (error.code === 'auth/too-many-requests') {
                  setMsg(label.errorMsgTooManyAttempts);
                } else {
                  console.error(error);
                  setMsg(label.errorMsgLoginUPWrong);
                }
              });
          } else setMsg(label.errorMsgNotActivated);
        } else setMsg(label.errorMsgUnregisteredEmail);
      }).catch(error => {
        console.log("Error: " + error.message);
        if (error.code === 'auth/too-many-requests') {
          setMsg(label.errorMsgTooManyAttempts);
        } else {
          setMsg(label.errorMsgGeneral);
        }
      });
    } else {
      setMsg(label.errorMsgLoginUPEmpty);
    }
    setLogging(false);
  }

  return (
    <Layout style={dstyles.container} height={height} msg={msg} callback={() => setMsg("")}>
      <View style={dstyles.bgTheme}>
        <Image source={img.header} style={styles.back} />
        <View style={styles.logoCont}>
          <Image source={img.logo} style={styles.logo} />
        </View>
      </View>
      <View style={styles.textCont}>
        <Text style={dstyles.title}>{label.welcomeCompany}</Text>
        <TextInput style={dstyles.input}
          placeholder={label.inputEmail}
          placeholderTextColor={Color.lightGrey}
          textContentType={Input.email}
          maxLength={200}
          value={u}
          onChangeText = {e => setU(e)} />
        <TextInput style={dstyles.input}
          placeholder={label.inputPassword}
          placeholderTextColor={Color.lightGrey}
          secureTextEntry={true}
          textContentType={Input.password}
          onChangeText = {e => setP(e)}
          value={p}
          maxLength={200} />
        <View style={dstyles.alignMiddle}>
          <Button type="solid" title={label.signin} buttonStyle={[dstyles.button, dstyles.bgSecondary]} onPress={login} />
        </View>
        <View style={[dstyles.row, dstyles.alignMiddle]}>
          <Text>New company? </Text><Text style={dstyles.link} onPress={() => navigation.navigate('CompanyRegister')}>{label.signup}</Text>
        </View>
        <View style={[dstyles.row, dstyles.alignMiddle]}>
          <Text style={dstyles.link} onPress={() => navigation.navigate('Login')}>{'Switch to Member Login'}</Text>
        </View>
      </View>
    </Layout>
  );
}

const styles = StyleSheet.create({
  logoCont: {
    position: 'absolute',
    top: 80,
    alignSelf: 'center',
    zIndex: 2,
  },
  logo: {
    width: 110,
    height: 110,
  },
  // logoCont: {
  //   flex: 1,
  //   flexDirection: 'row',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   height: 260,
  // },
  back: {
    minWidth: "100%",
    height: 310,
    zIndex: 1,
  },
  textCont: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'center',
    alignItems: 'center', // horizontal align
    justifyContent: 'center',
    marginTop: 0,
    width: 300,
    padding: Size.lg,
  },
  text: {
    fontSize: 14,
    color: '#ffffff'
  }
});

import React, { useState } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Button, Text } from 'react-native-elements';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Layout from '@layout/basic';
import { icons, img } from '@handler/img';
import Banner from '@elements/banner';
import CustomHeader from '@elements/header';
import { ImageInput, LabelInput, TextAreaInput } from '@elements/input';
import { Color, Size, dstyles, label } from '@handler/data';
import GlobalFunc from '@handler/functions';
import firebase, { FB_URL } from "../../config/firebase";
import auth from '@react-native-firebase/auth';
import storage from '@react-native-firebase/storage';
import AsyncStorage from '@react-native-community/async-storage';
import { setCommunity, setCommunityName, setCommunityImage, setCommunityContact, setCommunityDetails, setCommunityUrl, setCommunityLocation, setCommunityStartTime, setCommunityEndTime } from '@actions/communityActions';


function EditCommunityScreen(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [user, setUser] = useState({});
  const [adding, setAdding] = useState(false);
  const [updating, setUpdating] = useState(false);
  const [deleting, setDeleting] = useState(false);
  const [loading, setLoading] = useState(true);
  const [loadingText, setLoadingText] = useState('');
  const [msgTitle, setMsgTitle] = useState(false);
  const [msg, setMsg] = useState(false);
  const [errors, setErrors] = useState([]);
  const [image, setImage] = useState(null);
  const [comm, setComm] = useState(null);
  //const [uploading, setUploading] = useState(false);
  //const [transferred, setTransferred] = useState(0);
  const height = 940;
  const id = params && params.id ? params.id : null;
  const docDbRef = firebase.firestore().collection('communities');
  const imgRef = firebase.firestore().collection('images');
  const editCallback = params && params.callback ? params.callback : null;

  // validate inputs
  const validateInputs = function() {
    var errs = [];
    if (props.communityRed.community.name == '') {
      errs.push('Community Name cannot be empty.');
    }
    if (props.communityRed.community.contact == '') {
      errs.push('Community Email cannot be empty.');
    } else if (!/\w+@\w+\.\w+/.test(props.communityRed.community.contact)) {
      errs.push('Community Email format is wrong.');
    }
    if (props.communityRed.community.location == '') {
      errs.push('Community Location cannot be empty.');
    }
    if (props.communityRed.community.details == '') {
      errs.push('Community Details cannot be empty.');
    }
    // if (props.communityRed.community.name == '') {
    //   errs.push('Community Name cannot be empty.');
    // }
    return errs;
  }

  // save community
  const saveCommunity = async () => {
    var errs = validateInputs();

    if (errs.length > 0) { // Error
      setMsgTitle('Save Failed');
      setMsg('\n'+errs.join('\n'));
      setErrors(errs);

    } else { // Save
      var docData = null, docId = null;

      // read community data
      await docDbRef.doc(id).get().then(res => {
        docId = res.id;
        docData = res.data();
      }).catch(error => { console.log('Error: ' + error); });

      // handle image
      if (image && image.uri) { // if uploaded a new image
        console.log('uploading image');
        const uri = image.uri;
        const filename = auth().currentUser.uid + '_' + (image.fileName ? image.fileName : image.uri.substr(image.uri.lastIndexOf('/') + 1)); //uri.substring(uri.lastIndexOf('/') + 1);
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        // setTransferred(0);
        const task = storage().ref(filename).putFile(uploadUri);
        // set progress state
        // task.on('state_changed', snapshot => {
        //   setTransferred(
        //     Math.round(snapshot.bytesTransferred / snapshot.totalBytes) * 10000
        //   );
        // });
        try {
          await task;
        } catch (e) {
          console.error(e);
        }

        var img = null, imgId = null;

        // get image
        await imgRef.get().then(snapshot => {
          snapshot.forEach((res2) => {
            var imgdata = res2.data();
            if (imgdata.ref_id == 'communities/' + docId) {
              imgId = res2.id;
              img = imgdata;
              return false;
            }
          });
        }).catch(error => { console.log('Error: ' + error); });

        // edit image
        try {
          img.image = FB_URL + '' + filename;

          // save image
          await imgRef.doc(imgId).set(img).then(ret => {
            console.log('Image saved.');
          }).catch(error => { console.log('Error: ' + error); });
        } catch(err) {
          console.log('Image not found.');
        }
      }

      // save community data
      // set
      docData.contact = props.communityRed.community.contact;
      docData.description = props.communityRed.community.details;
      docData.end_time = props.communityRed.community.end_time;
      docData.is_approved = false;
      docData.location = props.communityRed.community.location;
      docData.name = props.communityRed.community.name;
      docData.registration_url = props.communityRed.community.url;
      docData.sequence = '';
      docData.start_time = props.communityRed.community.start_time;

      // write
      await docDbRef.doc(id).set(docData).then(docRef => {
        setUpdating(false);
        setMsgTitle('');
        setMsg(label.communitySaved);
      }).catch(error => {
        console.log('Error: ' + error);
        setUpdating(false);
        setMsgTitle('');
        setMsg(label.communitySaveFailed);
      });

      setErrors(false);

      if (typeof editCallback != undefined) {
        editCallback();
      }
    }
  }

  // add community
  const addCommunity = async () => {
    var errs = validateInputs();

    if (errs.length > 0) { // Error
      setAdding(false);
      setMsgTitle('Add Failed');
      setMsg('\n'+errs.join('\n'));
      setErrors(errs);

    } else { // Add
      let userData = await GlobalFunc.getUser({self: true});

      var docData = {
        business_owner_id: '',
        contact: props.communityRed.community.contact,
        description: props.communityRed.community.details,
        end_time: props.communityRed.community.end_time,
        is_active: true,
        is_approved: false,
        is_featured: false,
        location: props.communityRed.community.location,
        name: props.communityRed.community.name,
        registration_url: props.communityRed.community.url,
        sequence: '',
        start_time: props.communityRed.community.start_time,
        user_owner_id: 'users/' + userData.id,
      };

      var msg = '', docId = null;

      // write
      await docDbRef.add(docData).then(docRef => {
        msg = label.communityAdded;
        docId = docRef.id;
      }).catch(error => {
        console.log('Error: ' + error);
        msg = label.communityAddFailed;
      });

      // handle image
      if (image && image.uri) {
        console.log('uploading image');
        const uri = image.uri;
        const filename = auth().currentUser.uid + '_' + image.fileName;
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        const task = storage().ref(filename).putFile(uploadUri);
        try {
          await task;
        } catch (e) {
          console.error(e);
        }

        // create image
        var img;
        if (!prom) {
          img = {
            image: FB_URL + '' + filename,
            name: filename,
            ref_id: 'communities/' + docId,
            community_id: 'communities/' + docId,
            promotion_id: '',
            created_uid: 'users/' + userData.id,
            created_time: moment(),
            last_updated_uid: 'users/' + userData.id,
            last_updated_time: moment(),
            sequence: 1,
          }
        } else {
          img = {
            image: FB_URL + '' + filename,
            name: filename,
            ref_id: 'communities/' + docId,
            community_id: 'communities/' + docId,
            promotion_id: '',
            last_updated_uid: 'users/' + userData.id,
            last_updated_time: moment(),
            sequence: 1,
          }
        }

        // save image
        await imgRef.add(img).then(ret => {
          console.log('Image saved.');
        }).catch(error => { console.log('Error: ' + error); });
      }

      setAdding(false);
      setMsgTitle('');
      setMsg(msg);
      setErrors(false);

      if (typeof editCallback != undefined) {
        editCallback();
      }
    }
  }

  // delete community
  const deleteCommunity = async () => {
    console.log('Deleting data');
    var msg = '';
    var img = props.communityRed.community.image && props.communityRed.community.image.uri.includes('http') ? props.communityRed.community.image.uri : null;
    let userData = await GlobalFunc.getUser({self: true});
    var imgData = await GlobalFunc.getImage({userId: userData.id, refId: 'communities/' + params.id});

    // delete community
    await docDbRef.doc(id).delete().then(docRef => {
      msg = label.communityDeleted;
    }).catch(error => {
      console.log('Error: ' + error);
      msg = label.communityDeleteFailed;
    });

    // delete img
    if (imgData) {
      await imgRef.doc(imgData.id).delete().then(docRef => {
      }).catch(error => {
        console.log('Error: ' + error);
      });
    }

    setDeleting(false);
    setMsgTitle('');
    setMsg(msg);
    setErrors(false);

    if (typeof editCallback != undefined) {
      editCallback();
    }
  }

  const handleUploadPhoto = async (res) => {
    setImage(res);
    return true;

    // Local Server
    // fetch(uploadServer, {
    //   method: 'POST',
    //   body: createFormData(val, { userId: '123' }),
    // })
    // .then((response) => response.json())
    // .then((response) => {
    //   console.log('upload succes', response);
    //   alert('Upload success!');
    //   //setVal(null);
    // })
    // .catch((error) => {
    //   console.log('upload error', error);
    //   alert('Upload failed!');
    // });
  }

  const callback = function() {
    setMsg(false);
    setLoadingText('');
    if (!errors) navigation.goBack();
  }

  const addPressed = function() {
    setLoadingText(label.adding);
    setAdding(true);
  }

  const savePressed = function() {
    setLoadingText(label.updating);
    setUpdating(true);
  }

  const deletePressed = function() {
    setLoadingText(label.deleting);
    setDeleting(true);
  }

  var buttons;
  if (params && params.id) {
    if (loading) {
      async function setCommunityData() {
        let userData = await GlobalFunc.getUser({self: true});
        var communityData = {};

        if (userData) { // if user is logged in
          communityData = await GlobalFunc.getCommunity({userId: userData.id, communityId: params.id});
        }

        setLoading(false);
        setUser(userData);

        if (communityData) {
          setComm(communityData);
          props.setCommunity(communityData);
        }
      }
      setCommunityData();
    } else if (updating) {
      saveCommunity();
    } else if (deleting) {
      deleteCommunity();
    }

    buttons =
      <View style={dstyles.buttons}>
        <View style={[dstyles.row, dstyles.alignMiddle]}>
          <Button type="solid" title={label.save} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={savePressed} />
        </View>
        <View style={[dstyles.row, dstyles.alignMiddle]}>
          <Button type="solid" title={label.delete} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={deletePressed} />
        </View>
      </View>;
  } else {
    var title = 'New Community';
    if (loading) {
      props.setCommunity({name: '', image: null, contact: '', details: '', url: ''});
      setLoading(false);
    } else if (adding) {
      addCommunity();
    }

    buttons =
      <View style={dstyles.buttons}>
        <View style={[dstyles.row, dstyles.alignMiddle]}>
          <Button type="solid" title={label.addCommunity} buttonStyle={[dstyles.button2, dstyles.bgSecondary, {width: 200}]} onPress={addPressed} />
        </View>
      </View>;
  }

  var accepted = comm && comm.id ? 'status: ' + (props.communityRed.community.is_approved ? 'approved.' : 'not yet approved.') : null;

  return (
    loading ? <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.myep} options={{back: true, menu: false}} />
      <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{label.loading}</Text></View>
    </View> :
    (updating || adding || deleting ? <View style={dstyles.container}>
      <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{loadingText}</Text></View>
    </View> :
    <Layout style={dstyles.container} height={height} title={msgTitle} msg={msg} callback={callback}>
      <CustomHeader navigation={navigation} title={label.myep} options={{back: true, menu: false}} />
      <ScrollView style={dstyles.scrollViewContent}>
        <Banner title={title ? title : props.communityRed.community.name}></Banner>
        <View style={dstyles.tightContent}>
          <Text style={[dstyles.text, dstyles.textRight, {marginBottom: 20}]}>{accepted}</Text>
          <LabelInput title={label.name + "*"} value={props.communityRed.community.name} placeholder={label.inputName} onChange={(e) => props.setCommunityName(e)} />
          <ImageInput source={props.communityRed.community.image ? props.communityRed.community.image : img.empty_add} title={label.image} onChange={(e) => props.setCommunityImage(e)} width={600} height={450} handleUploadPhoto={handleUploadPhoto} />
          <LabelInput title={label.email + "*"} value={props.communityRed.community.contact} desc={label.exampleEmail} placeholder={label.inputEmail} onChange={(e) => props.setCommunityContact(e)} />
          <LabelInput title={label.url} value={props.communityRed.community.url} desc={label.exampleUrl} placeholder={label.inputURL} onChange={(e) => props.setCommunityUrl(e)} />
          <LabelInput title={label.location + "*"} value={props.communityRed.community.location} desc={label.exampleLocation} placeholder={label.inputLocation} onChange={(e) => props.setCommunityLocation(e)} />
          <LabelInput title={label.startTime} value={props.communityRed.community.start_time} desc={label.exampleTime} placeholder={label.inputStartTime} onChange={(e) => props.setCommunityStartTime(e)} />
          <LabelInput title={label.endTime} value={props.communityRed.community.end_time} desc={label.exampleTime} placeholder={label.inputEndTime} onChange={(e) => props.setCommunityEndTime(e)} />
          <TextAreaInput title={label.details + "*"} value={props.communityRed.community.details} placeholder={label.input} onChange={(e) => props.setCommunityDetails(e)} />
          { buttons }
        </View>
      </ScrollView>
    </Layout>
    )
  );
}

const styles = StyleSheet.create({
});

const mapStateToProps = (state) => {
  const { communityRed } = state
  return { communityRed }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    setCommunity,
    setCommunityName,
    setCommunityImage,
    setCommunityContact,
    setCommunityUrl,
    setCommunityLocation,
    setCommunityStartTime,
    setCommunityEndTime,
    setCommunityDetails,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(EditCommunityScreen);

import React, { useState } from 'react';
import { ScrollView, StyleSheet, TextInput, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements'; // styling see at react-native-elements doc
import Carousel from '@elements/carousel';
import Layout from '@layout/basic';
import { Color, Input, Size, dstyles, label } from '@handler/data';
import { img } from '@handler/img';
import GlobalFunc from '@handler/functions';


export default function PostersScreen(props) {
  const params = props.route.params;
  const navigation = props.navigation;
  const [loading, setLoading] = useState(true);
  const [posters, setPosters] = useState([]);
  const height = 500;

  if (loading) {
    async function setPosterData() {
      let posterData = await GlobalFunc.getPosters({navigation: navigation});
      setLoading(false);
      setPosters(posterData);
    }
    setPosterData();
  }

  return (
    <Layout style={dstyles.container} height={height}>
      <View style={[dstyles.bgTheme]}>
        <Image source={img.header} style={styles.back} />
        <View style={styles.logoCont}>
          {
            loading ? <Text>{'Loading...'}</Text> :
            <Carousel data={posters} nav={navigation} />
          }
        </View>
      </View>
      <View style={styles.textCont}>
        <View style={[dstyles.row, dstyles.alignMiddle]}>
          <Button type="solid" title={label.login} buttonStyle={[dstyles.button, dstyles.bgSecondary]} onPress={() => navigation.navigate('Login')} />
        </View>
      </View>
    </Layout>
  );
}

const styles = StyleSheet.create({
  logoCont: {
    position: 'absolute',
    top: 70,
    alignSelf: 'center',
    zIndex: 2,
  },
  back: {
    minWidth: "100%",
    height: 400,
    zIndex: 1,
  },
  textCont: {
    flexDirection: 'column',
    alignSelf: 'center',
    alignItems: 'center', // horizontal align
    justifyContent: 'center',
    marginTop: 0,
    width: 300,
    height:90,
    padding: Size.lg,
  },
  text: {
    fontSize: 14,
    color: '#ffffff'
  }
});

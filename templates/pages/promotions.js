import React, { useState } from 'react';
import { ActivityIndicator, RefreshControl, ScrollView, StyleSheet, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements';
import { icons, img } from '@handler/img';
import Banner from '@elements/banner';
import { PromotionListItem, SponsorListItem } from '@elements/listitem';
import CustomHeader from '@elements/header';
import { Color, Size, dstyles, label } from '@handler/data';
import GlobalFunc from '@handler/functions';
import firebase from "../../config/firebase";
import AsyncStorage from '@react-native-community/async-storage';


export default function PromotionsScreen(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [user, setUser] = useState({});
  const [sponsorList, setSponsorList] = useState([]);
  const [promoList, setPromoList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);


  const refresh = () => {
    setLoading(true);
  }

  if (loading) {
    async function setData() {
      let userData = await GlobalFunc.getUser({self: true});
      var promoData = [], sponsorData = [];

      if (userData) { // if user is logged in
        // set promo list
        promoData = await GlobalFunc.getPromoList({userId: userData.id, all: true, navigation: navigation});

        // set sponsor list
        sponsorData = await GlobalFunc.getSponsorList({userId: userData.id, navigation: navigation});
      }

      setLoading(false);
      setRefreshing(false);
      setUser(userData);

      if (promoData) setPromoList(promoData);
      else setPromoList(['error']);
      if (sponsorData) setSponsorList(sponsorData);
      else setSponsorList(['error']);
    }
    setData();
  }

  return (
    <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.app} />
      <ScrollView refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={refresh} />
      }>
        <Banner title={loading ? '' : label.welcomeb+'\n'+user.first_name} image={img.back2}></Banner>
        <View style={dstyles.content}>
          <Text style={dstyles.title}>{label.promotions}</Text>
          {
            loading ? <Text style={dstyles.loading}>{label.loading}</Text> :
            (
              promoList.length > 0 ?
              (
                promoList[0] == 'error' ? <Text style={dstyles.loading}>{label.errorLoadingData}</Text> :
                <View style={dstyles.listContainer}>
                  { promoList.map((l, i) => ( <PromotionListItem id={l.id} key={i} title={l.name} desc={l.description ? l.description : label.noDesc} options={{url: l.url}} image={l.image} /> )) }
                </View>
              ) :
              <Text style={dstyles.loading}>{label.noPromos}</Text>
            )
          }
        </View>
      </ScrollView>
      <ScrollView style={dstyles.sponsorListContainer} horizontal={true}>
        { sponsorList.map((l, i) => ( <SponsorListItem key={i} image={l.image} options={{url: l.url}} /> )) }
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

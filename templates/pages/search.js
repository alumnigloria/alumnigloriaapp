import React, { useState } from 'react';
import { RefreshControl, ScrollView, StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { CommonActions, StackActions } from '@react-navigation/native';
import CustomHeader from '@elements/header';
import { SearchListItem } from '@elements/listitem';
import { Color, Size, dstyles, label } from '@handler/data';
import GlobalFunc from '@handler/functions';
import { icons, img } from '@handler/img';
import { searchAct, setSearchName } from '@actions/searchActions';

var sPpl, sCom, sType, sName, sOcc;

function SearchScreen(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [user, setUser] = useState({});
  const [setup, setSetup] = useState(true);
  const [loading, setLoading] = useState(true);
  const [users, setUsers] = useState([]);
  const [coms, setComs] = useState([]);
  const [result, setResult] = useState({});
  const [refreshing, setRefreshing] = useState(false);

  const refresh = () => {
    setLoading(true);
    search(true);
  }

  sName = props.searchRed.search.name.toLowerCase();
  sType = props.searchRed.search.type.toLowerCase();
  sOcc = props.searchRed.search.job.toLowerCase();

  // Search details
  const searchDetails = function(data, count = 0) {
    var res = [];

    data.forEach(function(d) {
      var dName = d.name ? d.name.toLowerCase() : '';
      var dOcc = d.occupation ? d.occupation.toLowerCase() : '';

      if (!sName || dName.indexOf(sName) > -1) {
        if (!sOcc || dOcc.indexOf(sOcc) > -1) {
          res.push(d);
          count++;
        }
      }
    });

    return {data: res, count: count};
  }

  // Search
  const search = function(click = false) { // click = seardh from this page
    var res = {};

      var people = label.people.toLowerCase(),
          company = label.company.toLowerCase(),
          all = label.all.toLowerCase(),
          sppltemp = 0, scomtemp = 0;

      if ((params && params.search == true) || click) {
        sPpl = 0; sCom = 0;
      }

      if (sType != people && sType != all) sPpl = -1; // hide users
      if (sType != company && sType != all) sCom = -1; // hide companies

      if (sType == people || sType == all) {
        var r = searchDetails(users, sPpl);
        res.users = r.data;
        sPpl = r.count;
      }
      if (sType == company || sType == all) {
        var r = searchDetails(coms, sCom);
        res.companies = r.data;
        sCom = r.count;
      }

    setResult(res);
    setLoading(false);
  }

  const resetSearch = function() {
    sName = '';
    sOcc = '';
    props.searchRed.search.name = '';
    props.searchRed.search.job = '';
    props.searchRed.search.type = label.all;
  }

  const itemClicked = function(id, type) {
    if (type == 'people') navigation.navigate('ProfileScreen', {first: false, userId: id});
    else navigation.navigate('CompanyProfileScreen', {first: false, comId: id});
  }

  if (setup) {
    async function setUserComData() {
      let userData = await GlobalFunc.getUser({self: true});
      var usersData = [], comsData = [];

      if (userData) { // if user is logged in
        // set users list
        usersData = await GlobalFunc.getAllUsers({userId: userData.id, all: true, navigation: navigation});

        // set companies list
        comsData = await GlobalFunc.getAllCompanies({userId: userData.id, navigation: navigation});
      }

      resetSearch();
      setSetup(false);
      setUser(userData);

      if (usersData) setUsers(usersData);
      else setUsers(['error']);
      if (comsData) setComs(comsData);
      else setComs(['error']);
    }
    setUserComData();

  } else {
    if (loading && users.length > 0 && coms.length > 0) {
      search(false);
    }
    if (params && params.search == true) {
      params.search = false;
    } else if (params && params.first == true) {
      resetSearch();
    }
  }


  return (
    loading ? <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.app} />
      <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{label.loading}</Text></View>
    </View> :
    <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.app} />
      <ScrollView refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={refresh} />
      }>
        <View style={dstyles.bgTheme}>
          <Image source={img.back2} style={styles.img} />
          <View style={styles.bannerText}>
            <Text style={[dstyles.title, dstyles.textWhite]}>{label.search}</Text>
            <TextInput style={dstyles.input} placeholder={label.input} value={props.searchRed.search.name} onChangeText={e => props.setSearchName(e) } onBlur={e => {search(true);}} />
            <TouchableOpacity color="black" type="clear" style={styles.searchButton} onPress={e => { search(true); }}>
              <Image source={icons.search} style={styles.searchIcon} />
            </TouchableOpacity>
            <Text style={[dstyles.textCenter, dstyles.textWhite, dstyles.dnone]}>{label.type + ': ' + props.searchRed.search.type}</Text>
            <TouchableOpacity color="black" type="clear" style={styles.arrowButton} onPress={() => { navigation.navigate('SearchNavigation', {screen: 'SearchDetails', params: {callback: refresh}}); } }>
              <Image source={img.arrow} style={styles.arrow} />
            </TouchableOpacity>
          </View>
        </View>
        <Text style={[dstyles.filledTitle, sPpl < 0 ? dstyles.dnone : null]}>{label.people}</Text>
        <View style={sPpl < 0 ? dstyles.dnone : null}>
          { result.users ? result.users.map((l, i) => { return ( <SearchListItem key={i} name={l.name} click={() => itemClicked(l.id, 'people')} />); }) : null }
          { sPpl == 0 ? <SearchListItem key={0} name={'No people found'} center /> : null }
        </View>
        <Text style={[dstyles.filledTitle, sCom < 0 ? dstyles.dnone : null]}>{label.company}</Text>
        <View style={sCom < 0 ? dstyles.dnone : null}>
          { result.companies ? result.companies.map((l, i) => { return ( <SearchListItem key={i} name={l.name} click={() => itemClicked(l.id, 'company')} />); }) : null }
          { sCom == 0 ? <SearchListItem key={0} name={'No company found'} center /> : null }
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  img: {
    minWidth: "100%",
    height: 150,
    opacity: 0.1,
    zIndex: 1,
  },
  bannerText: {
    flex:1,
    top: 0,
    width: "100%",
    position: 'absolute',
    zIndex: 2,
    height: 130,
    alignSelf: 'stretch',
    textAlign: 'center',
    textAlignVertical:'center',
    paddingTop: Size.sm,
    paddingHorizontal: Size.xxl,
  },
  title: {
    color:Color.white,
    fontSize:Size.fxlarge,
  },
  arrowButton: {
    marginTop: Size.md,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  searchButton: {
    position: 'absolute',
    right: 46,
    top: 52,
    flex: 1,
    padding: Size.xs,
    display: 'flex',
    zIndex: 3,
  },
  searchIcon: {
    width: 27,
    height: 24,
  },
  arrow: {
    width: 30,
    height: 16,
  },
});

const mapStateToProps = (state) => {
  const { searchRed } = state
  return { searchRed }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    searchAct,
    setSearchName,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);

import React, { useState } from 'react';
import { ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Button, Text } from 'react-native-elements';
import { icons, img } from '@handler/img';
import CustomHeader from '@elements/header';
import { Color, Size, dstyles, label } from '@handler/data';


export default function CommunitiesScreen(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [activeIndex, setActiveIndex] = useState(0);

  return (
    <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.app} />
      <ScrollView>
        <View style={dstyles.content}>
          <Text style={dstyles.title}>{label.settings}</Text>
          <Text>{label.test}</Text>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
});

import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements';
import { StackActions } from '@react-navigation/native';
import { Color, Size, dstyles, label } from '@handler/data';
import { img } from '@handler/img';
import { setState, setStateInit, setStateUser } from '@actions/initActions';


function LoadingScreen(props) {
  const params = props.route.params;
  var text = params && params.text ? params.text : 'Loading Data...';

  useEffect(function() {
    setTimeout(function() {
      if (props.initRed) {
        if (props.initRed.init.user)
          props.navigation.dispatch(StackActions.replace('Main'));
        else props.navigation.dispatch(StackActions.replace('Posters'));
      }
    }, 1000);
  }, [props.initRed.init]);
  

  return (
    <View style={[dstyles.container, dstyles.bgTheme]}>
      <View style={dstyles.fullScreen}>
        <Image source={img.logo} style={dstyles.logo} />
      </View>
      <View style={styles.textCont}>
        <Text style={styles.text}>{text}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  textCont: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center', // horizontal align
    marginTop: 0,
  },
  text: {
    fontSize: 14,
    color: '#ffffff'
  }
});

const mapStateToProps = (state) => {
  const { initRed } = state
  return { initRed }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    setState,
    setStateInit,
    setStateUser,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(LoadingScreen);

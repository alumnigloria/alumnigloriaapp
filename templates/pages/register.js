import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, TextInput, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements'; // styling see at react-native-elements doc
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Layout from '@layout/basic';
import { LabelInput } from '@elements/input';
import { Color, Input, Size, dstyles, label } from '@handler/data';
import { img } from '@handler/img';
import { sha256 } from 'react-native-sha256';
import GlobalFunc from '@handler/functions';
import { resetUserData,setUser, setUserEmail, setUserPassword, setUserFirstName, setUserLastName, setUserGrad, setUserTelp } from '@actions/userActions';
import moment from 'moment';


function RegisterScreen(props) {
  const params = props.route.params;
  const navigation = props.navigation;
  const [msgTitle, setMsgTitle] = useState(false);
  const [msg, setMsg] = useState(false);
  const height = 800;
  const [loadingText, setLoadingText] = useState('');
  const [adding, setAdding] = useState(false);
  const [errors, setErrors] = useState([]);
  const [addResult, setAddResult] = useState(true);
  const [setup, setSetup] = useState(true);
  const editCallback = params && params.callback ? params.callback : null;

  // validate inputs
  const validateInputs = function() {
    var errs = [];
    if (props.userRed.user.first_name == '') {
      errs.push(label.errorMsgFirstNameEmpty);
    }
    if (props.userRed.user.last_name == '') {
      errs.push(label.errorMsgLastNameEmpty);
    }
    if (props.userRed.user.email != '' && !/\w+@\w+\.\w+/.test(props.userRed.user.email)) {
      errs.push(label.errorMsgEmailFormat);
    }
    if (props.userRed.user.password == '') {
      errs.push(label.errorMsgPasswordEmpty);
    } else if (props.userRed.user.password.length < 6 || !/(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}/g.test(props.userRed.user.password)) {
      errs.push(label.errorMsgPasswordReq);
    }
    if (props.userRed.user.graduation == '') {
      errs.push(label.errorMsgGradEmpty);
    }
    return errs;
  }

  // register
  const memberRegistration = async () => {
    var errs = validateInputs();

    if (errs.length > 0) { // Error
      setMsgTitle(label.errorMsgUserRegisterFailed);
      setMsg('\n'+errs.join('\n'));
      setLoadingText('');
      setErrors(errs);
      setAddResult(true);

    } else {
      var existingUser = await GlobalFunc.checkUserExists({email: props.userRed.user.email, type:'users'});
      if (existingUser === null || existingUser.length > 0) {
        errs.push(label.errorMsgEmailExist);  
      }

      // sha256(props.userRed.user.password).then( hash => {
      //   props.setUserPassword(hash);
      // });

      var docData = {
        is_active: true,
        is_approved: false,
        is_staff: false,
        is_super_user: false,
        email: props.userRed.user.email.toLowerCase(),
        password: props.userRed.user.password,
        first_name: props.userRed.user.first_name,
        last_name: props.userRed.user.last_name,
        year_of_graduation: props.userRed.user.graduation,
        contact_number: props.userRed.user.telp,
        biography: '',
        birth_date: '',
        business_ids: '',
        connection_ids: '',
        current_occupation: '',
        instagram: '',
        linkedin: '',
        location: '',
        facebook_url: '',
        profile_picture: '',
        profile_preference: 'public',
        sponsorship: 'none',
        sponsor_order: 0,
        last_seen: moment(new Date()).format('DD MMM YYYY hh:mm'),
        last_updated_time: moment(new Date()).format('DD MMM YYYY hh:mm'),
        last_updated_uid: '',
        created_time: moment(new Date()).format('DD MMM YYYY hh:mm'),
        created_uid: '',
      };

      var msg = '', docId = null;

      // write
      [docId, msg] = await GlobalFunc.addUser(docData);

      setMsgTitle('');
      setMsg(docId ? "Member registration request sent, please wait until the registration approved." : msg);
      setLoadingText('');
      setErrors(docId ? false : true);

      if (typeof editCallback === 'function') {
        editCallback();
      }
      setAddResult(docId ? docId : true);
    }
    setAdding(false);
  }

  const callback = function() {
    setMsg(false);
    setLoadingText('');
    if (!errors) navigation.goBack();
  }

  const addPressed = function() {
    setLoadingText('Registering a new user...');
    setAdding(true);
  }

  useEffect(function() {
    if (setup) {
      setSetup(false);
      props.resetUserData();
      setAddResult(true);
    }
    if (adding && addResult) {
      setAddResult(false);
      setTimeout(function() { memberRegistration(); }, 1000);
      
    }
  }, [setup, adding]);

  return (
    adding || !addResult ? <View style={dstyles.container}>
      <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{loadingText}</Text></View>
    </View> :
    <Layout style={dstyles.container} height={height} title={msgTitle} msg={msg} callback={callback}>
      <View style={{display: 'flex'}}>
        <View style={[dstyles.tightContent, dstyles.bgTheme, {paddingTop: Size.xxl, paddingBottom: Size.xxl}]}>
          <Text style={[dstyles.title, dstyles.textWhite]}>{'New Member Registration'}</Text>
          <View>
            <LabelInput title={label.email + "*"} placeholder={label.inputEmail} type={2} onChange={(e) => props.setUserEmail(e)} value={props.userRed.user.email} />
            <LabelInput title={label.password + "*"} placeholder={label.inputPassword} secureTextEntry={true} type={2} onChange={(e) => props.setUserPassword(e)} value={props.userRed.user.password} />
            <LabelInput title={label.first_name + "*"} placeholder={label.inputFirstName} type={2} onChange={(e) => props.setUserFirstName(e)} value={props.userRed.user.first_name} />
            <LabelInput title={label.last_name + "*"} placeholder={label.inputLastName} type={2} onChange={(e) => props.setUserLastName(e)} value={props.userRed.user.last_name} />
            <LabelInput title={label.grad + "*"} placeholder={label.inputGrad} type={2} onChange={(e) => props.setUserGrad(e)} value={props.userRed.user.graduation} />
            <LabelInput title={label.telp} placeholder={label.inputTelp} type={2} onChange={(e) => props.setUserTelp(e)} value={props.userRed.user.telp} />
          </View>
        </View>
      </View>
      <View style={styles.textCont}>
        <View style={dstyles.alignMiddle}>
          <Button type="solid" title={label.register} buttonStyle={[dstyles.button, dstyles.bgSecondary]} onPress={addPressed} />
        </View>
        <View style={dstyles.alignMiddle}>
          <Button type="solid" title={label.back} buttonStyle={[dstyles.button, dstyles.bgSecondary]} onPress={() => navigation.goBack()} />
        </View>
      </View>
    </Layout>
  );
}

const styles = StyleSheet.create({
  logo: {
    flex:1,
    position: 'absolute',
    width: '100%',
    height: 90,
    top: 0,
    alignSelf: 'center',
    zIndex: 2,
  },
  logoCont: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 260,
  },
  back: {
    minWidth: "100%",
    height: 260,
    opacity: 0.1,
    zIndex: 1,
  },
  textCont: {
    flexDirection: 'column',
    alignSelf: 'center',
    alignItems: 'center', // horizontal align
    justifyContent: 'center',
    marginTop: 0,
    width: 300,
    padding: Size.lg,
  },
  text: {
    fontSize: 14,
    color: '#ffffff'
  }
});

const mapStateToProps = (state) => {
  const { userRed } = state
  return { userRed }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    resetUserData,
    setUser,
    setUserEmail,
    setUserPassword,
    setUserFirstName,
    setUserLastName,
    setUserGrad,
    setUserTelp,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);

import React, { useState } from 'react';
import { ScrollView, StyleSheet, TextInput, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import firebase, { FB_URL } from "@config/firebase";
import auth from '@react-native-firebase/auth';
import storage from '@react-native-firebase/storage';
import Layout from '@layout/basic';
import { icons, img } from '@handler/img';
import Banner from '@elements/banner';
import CustomHeader from '@elements/header';
import { LabelInput, PickerInput } from '@elements/input';
import { Color, Size, dstyles, label } from '@handler/data';
import GlobalFunc from '@handler/functions';


export default function AddJobScreen(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [companies, setCompanies] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadingText, setLoadingText] = useState('');
  const [adding, setAdding] = useState(false);
  const [job, setJob] = useState('');
  const [company, setCompany] = useState('');
  const [startYear, setStartYear] = useState('');
  const [endYear, setEndYear] = useState('');
  const [errors, setErrors] = useState([]);
  const [msg, setMsg] = useState(false);
  const [msgTitle, setMsgTitle] = useState(false);
  const height = 400;
  const editCallback = params && params.callback ? params.callback : null;
  var docDbRef = null;

  // validate inputs
  const validateInputs = function() {
    console.log("TESTTTT");console.log(company);
    var errs = [];
    if (!job) {
      errs.push('Job name is required.');
    }
    if (!company) {
      errs.push('Job\'s company name is required.');
    }
    if (startYear == '') {
      errs.push('Job start year is required.');
    } else if (!/^[0-9]*$/.test(startYear)) {
      errs.push('Job start year must be a number.');
    }
    if (endYear != '' && !/\d+/.test(endYear)) {
      errs.push('Job end year must be a number.');
    }
    // if (endYear == '') {
    //   errs.push('Job end year is required.');
    // }
    return errs;
  }

  if (loading) {
    async function setUserData() {
      let userData = await GlobalFunc.getUser({self: true});
      var selfData = [], coms = [];

      if (userData) { // if user is logged in
        selfData = await GlobalFunc.getAllCompanies({userId: userData.id});

        selfData.forEach((item, i) => {
          coms.push({
            label: item.name,
            value: item.name.replace(/ /, '_').toLowerCase(),
          });
        });
      }

      setLoading(false);
      setCompanies(coms);
    }
    setUserData();
  }

  const addAJob = async function() {
    if (adding) return; 
    var errs = validateInputs();
    setAdding(true);

    if (errs.length > 0) { // Error
      setAdding(false);
      setMsgTitle('Add Failed');
      setMsg('\n'+errs.join('\n'));
      setErrors(errs);

    } else { // Add
      let userData = await GlobalFunc.getUser({self: true});
      docDbRef = firebase.firestore().collection('users').doc(userData.id).collection('jobs');

      var docData = {
        name: job,
        company: company,
        start_year: startYear,
        end_year: endYear,
        // user_owner_id: 'users/' + userData.id,
      };

      var msg = '', docId = null;

      // write
      await docDbRef.add(docData).then(docRef => {
        msg = label.jobAdded;
        docId = docRef.id;
      }).catch(error => {
        console.log('Error: ' + error);
        msg = label.jobAddFailed;
      });

      setMsgTitle('');
      setMsg(msg);
      setErrors(false);

      if (typeof editCallback != undefined) {
        editCallback();
      }
    }
    setAdding(false);
  }

  const callback = function() {
    setMsg(false);
    setLoadingText('');
    if (!errors) navigation.goBack();
  }


  return (
    loading ? <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.profile} options={{back: true, menu: false}} />
      <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{label.loading}</Text></View>
    </View> :
    <Layout style={dstyles.container} height={height} title={msgTitle} msg={msg} callback={callback} full={true}>
      <CustomHeader navigation={navigation} title={label.profile} options={{back: true, menu: false}} />
      <ScrollView style={[dstyles.scrollViewContent, dstyles.bgTheme]}>
        <View style={dstyles.bgTheme}>
          <View style={dstyles.tightContent}>
            <Text style={[dstyles.title, dstyles.textWhite]}>{label.addJob}</Text>
            <LabelInput title={label.job + "*"} placeholder={label.inputJob} type={2} inline={true} onChange={(e) => setJob(e)} value={job} />
            <LabelInput title={label.companyName + "*"} placeholder={label.inputCompanyName} type={2} inline={true} onChange={(e) => setCompany(e)} value={company} />
            <LabelInput title={label.startYear + "*"} placeholder={label.inputStartYear} type={2} inline={true} onChange={(e) => setStartYear(e)} value={startYear} />
            <LabelInput title={label.endYear} placeholder={label.inputEndYear} type={2} inline={true} onChange={(e) => setEndYear(e)} value={endYear} />
          </View>
          <View style={[dstyles.alignMiddle, {marginBottom: Size.lg}]}>
            <Button type="solid" title={label.addJob} buttonStyle={[dstyles.button2, dstyles.bgThemeDark, {width: 150}]} onPress={addAJob} />
          </View>
        </View>
      </ScrollView>
    </Layout>
  );
  // <PickerInput title={label.type + "*"} values={companies} type={2} inline={true} onChange={(v,i) => setCompany(v)} selectedValue={company} />
}

const styles = StyleSheet.create({
  selectArrowCont: {
    position: 'absolute',
    top:66,
    right: 45,
    zIndex: 3,
  },
  selectArrow: {
    width: 12,
    height: 8,
  }
});

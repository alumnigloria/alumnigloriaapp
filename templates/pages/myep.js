import React, { useState } from 'react';
import { RefreshControl, ScrollView, StyleSheet, View } from 'react-native';
import { Button, Text } from 'react-native-elements';
import { icons, img } from '@handler/img';
import Banner from '@elements/banner';
import CustomHeader from '@elements/header';
import { CommunityListItem, PromotionListItem } from '@elements/listitem';
import { Color, Size, dstyles, label } from '@handler/data';
import GlobalFunc from '@handler/functions';
import firebase from "../../config/firebase";


export default function MyEPScreen(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [user, setUser] = useState({});
  const [communityList, setCommunityList] = useState([]);
  const [promoList, setPromoList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);

  const refresh = () => {
    setLoading(true);
  }

  if (loading) {
    async function setData() {
      let userData = await GlobalFunc.getUser({self: true});
      var communityList = [], promoList = [];

      if (userData) { // if user is logged in
        // set community list
        communityList = await GlobalFunc.getCommunityList({userId: userData.id, navigation: navigation, callback: refresh });
        // set promo list
        promoList = await GlobalFunc.getPromoList({userId: userData.id, navigation: navigation, callback: refresh });
      }

      setLoading(false);
      setUser(userData);

      if (communityList) setCommunityList(communityList);
      else setCommunityList(['error']);
      if (promoList) setPromoList(promoList);
      else setPromoList(['error']);
    }
    setData();
  }

  return (
    <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.app} />
      {
        loading ? <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{label.loading}</Text></View> :
        <ScrollView refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={refresh} />
        }>
          <Banner title={label.welcomeb+'\n'+user.first_name} image={img.back2}></Banner>

          <View style={dstyles.content}>
            <Text style={dstyles.title}>{label.mypromos}</Text>
            <View style={[dstyles.listContainer, {paddingBottom: Size.xs}]} horizontal={true}>
              { promoList.length > 0 ? promoList.map((l, i) => ( <PromotionListItem id={l.id} key={i} title={l.name ? l.name : ' '} desc={l.desc ? l.desc : label.noDesc} options={{url: l.url, showApproved: true}} image={l.image} /> )) : <Text style={[dstyles.loading, {marginTop: Size.sm}]}>No promotion yet.</Text> }
            </View>
            <View style={[dstyles.row, dstyles.alignMiddle]}>
              <Button type="solid" title={label.addPromo} buttonStyle={[dstyles.button2, dstyles.bgSecondary, {width: 150}]} onPress={() => navigation.navigate('MyEPNavigation', {screen: 'EditPromotion', params:{id: 0, callback: refresh}})} />
            </View>

            <Text style={[dstyles.title, {marginTop: Size.sm}]}>{label.mycommunities}</Text>
            <View style={[dstyles.listContainer, {paddingBottom: Size.sm}]} horizontal={true}>
              { communityList.length > 0 ? communityList.map((l, i) => ( <CommunityListItem id={l.id} key={i} title={l.name} desc={l.desc ? l.desc : label.noDesc} options={{url: l.url, showApproved: true}} image={l.image} /> )) : <Text style={[dstyles.loading, {marginTop: Size.xs}]}>No community yet.</Text> }
            </View>
            <View style={[dstyles.row, dstyles.alignMiddle]}>
              <Button type="solid" title={label.addCommunity} buttonStyle={[dstyles.button2, dstyles.bgSecondary, {width: 200}]} onPress={() => navigation.navigate('MyEPNavigation', {screen: 'EditCommunity', params:{id: 0, callback: refresh}})} />
            </View>
          </View>
        </ScrollView>
      }
    </View>
  );
}

const styles = StyleSheet.create({
});

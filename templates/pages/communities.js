import React, { useState } from 'react';
import { ActivityIndicator, RefreshControl, ScrollView, StyleSheet, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements';
import { icons, img } from '@handler/img';
import Banner from '@elements/banner';
import CustomHeader from '@elements/header';
import { CommunityListItem, SponsorListItem } from '@elements/listitem';
import { Color, Size, dstyles, label } from '@handler/data';
import GlobalFunc from '@handler/functions';
import firebase from "../../config/firebase";
import AsyncStorage from '@react-native-community/async-storage';


export default function CommunitiesScreen(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [user, setUser] = useState({});
  const [sponsorlist, setSponsorList] = useState([]);
  const [communityList, setCommunityList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);

  const refresh = () => {
    setLoading(true);
  }

  if (loading) {
    async function setData() {
      let userData = await GlobalFunc.getUser({self: true});
      var communityData = [], sponsorData = [];

      if (userData) { // if user is logged in
        // set community list
        communityData = await GlobalFunc.getCommunityList({userId: userData.id, all: true, navigation: navigation});

        // set sponsor list
        sponsorData = await GlobalFunc.getSponsorList({userId: userData.id, navigation: navigation});
      }

      setLoading(false);
      setRefreshing(false);
      setUser(userData);

      if (communityData) setCommunityList(communityData);
      else setCommunityList(['error']);
      if (sponsorData) setSponsorList(sponsorData);
      else setSponsorList(['error']);
    }
    setData();
  }

  return (
    <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.app} />
      <ScrollView refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={refresh} />
      }>
        <Banner title={loading ? '' : label.welcomeb+'\n'+user.first_name} image={img.back2}></Banner>
        <View style={dstyles.content}>
          <Text style={dstyles.title}>{label.communities}</Text>
          {
            loading ? <Text style={dstyles.loading}>{label.loading}</Text> :
            <View style={dstyles.listContainer}>
              { communityList.map((l, i) => ( <CommunityListItem id={l.id} key={i} title={l.name} desc={l.description ? l.description : label.noDesc} options={{url: l.url}} image={l.image} /> )) }
            </View>
          }
        </View>
      </ScrollView>
      <ScrollView style={dstyles.sponsorListContainer} horizontal={true}>
        { sponsorlist.map((l, i) => ( <SponsorListItem key={i} image={l.image} options={{url: l.url}} /> )) }
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

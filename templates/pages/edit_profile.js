import React, { useState, useEffect } from 'react';
import { ScrollView, View, StyleSheet } from 'react-native';
import { Button, Text } from 'react-native-elements';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Layout from '@layout/basic';
import { icons, img } from '@handler/img';
import Banner from '@elements/banner';
import CustomHeader from '@elements/header';
import { ImageInput, LabelInput, TextAreaInput } from '@elements/input';
import { Color, Size, dstyles, label } from '@handler/data';
import GlobalFunc from '@handler/functions';
import firebase, { FB_URL } from "../../config/firebase";
import auth from '@react-native-firebase/auth';
import storage from '@react-native-firebase/storage';
import { setProfile, setProfileName, setProfileImage, setProfileOccupation, setProfileGrad, setProfileYear, setProfileEmail, setProfileTelp, setProfileAddress, setProfileFacebook, setProfileInstagram, setProfileLinkedin } from '@actions/profileActions';


function EditProfile(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [msgTitle, setMsgTitle] = useState(false);
  const [msg, setMsg] = useState(false);
  const [loading, setLoading] = useState(true);
  const [loadingText, setLoadingText] = useState('');
  const [first, setFirst] = useState(true);
  const [updating, setUpdating] = useState(false);
  const [errors, setErrors] = useState([]);
  const [image, setImage] = useState(null);
  const [jobs, setJobs] = useState([]);
  const [deletedJobs, setDeletedJobs] = useState([]);
  const height = 1020;
  const id = params && params.id ? params.id : null;
  const docDbRef = firebase.firestore().collection('users');
  const editCallback = params && params.callback ? params.callback : null;

  const refresh = () => {
    setLoading(true);
    setFirst(true);
  }

  // validate inputs
  const validateInputs = function() {
    var errs = [];
    if (props.profileRed.profile.name == '') {
      errs.push('Profile Name cannot be empty.');
    }
    if (props.profileRed.profile.email == '') {
      errs.push('Profile Email cannot be empty.');
    } else if (!/\w+@\w+\.\w+/.test(props.profileRed.profile.email)) {
      errs.push('Profile Email format is wrong.');
    }
    return errs;
  }

  const saveProfile = async () => {
    var errs = validateInputs();

    if (errs.length > 0) {
      setMsgTitle('Save Failed');
      setMsg('\n'+errs.join('\n'));
      setErrors(errs);
    } else {
      var docData = null, docId = null;

      let userData = await GlobalFunc.getUser({self: true});

      // read user data
      await docDbRef.doc(userData.id).get().then(res => {
        docId = res.id;
        docData = res.data();
      }).catch(error => { console.log('Error: ' + error); });

      // handle image
      if (image && image.uri) { // if uploaded a new image
        console.log('uploading image');
        const uri = image.uri;
        const filename = auth().currentUser.uid + '_' + image.fileName;
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        const task = storage().ref(filename).putFile(uploadUri);
        try {
          await task;
        } catch (e) {
          console.error(e);
        }

        docData.profile_picture = FB_URL + '' + filename;
      }

      // save user data
      // set
      docData.name = props.profileRed.profile.name;
      docData.occupation = props.profileRed.profile.occupation;
      docData.grad = props.profileRed.profile.grad;
      docData.address = props.profileRed.profile.address;
      docData.email = props.profileRed.profile.email;
      docData.contact_number = props.profileRed.profile.telp;
      docData.facebook_url = props.profileRed.profile.facebook;
      docData.instagram = props.profileRed.profile.instagram;
      docData.linkedin = props.profileRed.profile.linkedin;

      // write
      await docDbRef.doc(docId).set(docData).then(docRef => {
        deletedJobs ? deletedJobs.forEach(async (item, i) => {
          await docDbRef.doc(docId).collection('jobs').doc(item).delete();
        }) : null;
        setUpdating(false);
        setMsgTitle('');
        setMsg(label.profileSaved);
      }).catch(error => {
        console.log('Error: ' + error);
        setUpdating(false);
        setMsgTitle('');
        setMsg(label.profileFailed);
      });

      setErrors(false);

      if (typeof editCallback != undefined) {
        editCallback();
      }
    }
  }

  const callback = function() {
    setMsg(false);
    setLoadingText('');
    if (!errors) navigation.goBack();
  }

  const addAJob = function() {
    navigation.navigate('ProfileNavigation', {screen: 'AddJob', params: {callback: refresh}});
  }

  const savePressed = function() {
    setLoadingText(label.updating);
    setUpdating(true);
  }

  const handleUploadPhoto = async (res) => {
    setImage(res);
    return true;
  }

  useEffect(function() {
    if (loading && first) {
      setFirst(false);
      async function setUserData() {
        let userData = await GlobalFunc.getUser({self: true});
        var selfData = {};

        if (userData) { // if user is logged in
          selfData = await GlobalFunc.getUser({userId: userData.id});
        }
        props.setProfile(selfData);

        setJobs(selfData.jobs);
        setLoading(false);
      }
      setUserData();
    } else if (updating) {
      saveProfile();
    }
  });

  const deleteJob = async(i) => {
    var j = jobs, d = deletedJobs;
    d.push(j[i].id);
    j.splice(i,1);
    setJobs([]);
    setDeletedJobs(d);
    setTimeout(function() {setJobs(j)}, 10);
  }

  return (
    loading ?
    <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.app} />
      <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{label.loading}</Text></View>
    </View> :
    (updating ? <View style={dstyles.container}>
      <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{loadingText}</Text></View>
    </View> :
    <Layout style={dstyles.container} height={height} msg={msg} callback={callback}>
      <CustomHeader navigation={navigation} title={label.profile} options={{back: true, menu: false}} />
      <ScrollView style={dstyles.scrollViewContent}>
        <View style={dstyles.bgTheme}>
          <View style={[dstyles.tightContent, {paddingTop: Size.lg, paddingBottom: Size.xl}]}>
            <ImageInput source={{uri: props.profileRed.profile.profpic}} title={label.image} circle={true} onChange={(e) => props.setProfileImage(e)} handleUploadPhoto={handleUploadPhoto} />
            <Text style={[dstyles.pageTitle, dstyles.textWhite]}>{props.profileRed.profile.name}</Text>
            <LabelInput title={label.name + "*"} value={props.profileRed.profile.name} placeholder={label.inputName} type={2} onChange={(e) => props.setProfileName(e)} />
            <LabelInput title={label.occupation} value={props.profileRed.profile.occupation} placeholder={label.inputOccupation} type={2} onChange={(e) => props.setProfileOccupation(e)} />
            <LabelInput title={label.grad} value={props.profileRed.profile.graduation} placeholder={label.inputGrad} type={2} onChange={(e) => props.setProfileGrad(e)} />
            <LabelInput title={label.email + "*"} value={props.profileRed.profile.email} placeholder={label.inputEmail} type={2} onChange={(e) => props.setProfileEmail(e)} />
            <LabelInput title={label.telp} value={props.profileRed.profile.contact_number} placeholder={label.inputTelp} type={2} onChange={(e) => props.setProfileTelp(e)} />
            <LabelInput title={label.address} value={props.profileRed.profile.address} placeholder={label.inputAddress} type={2} onChange={(e) => props.setProfileAddress(e)} />
            <LabelInput title={label.fb} value={props.profileRed.profile.facebook} placeholder={label.inputFb} type={2} onChange={(e) => props.setProfileFacebook(e)} />
            <LabelInput title={label.insta} value={props.profileRed.profile.instagram} placeholder={label.inputInsta} type={2} onChange={(e) => props.setProfileInstagram(e)} />
            <LabelInput title={label.linkedIn} value={props.profileRed.profile.linkedin} placeholder={label.inputLinkedIn} type={2} onChange={(e) => props.setProfileLinkedin(e)} />
            <Text style={[dstyles.label, dstyles.textWhite]}>{label.jobs}</Text>
            <View>{
              jobs.map((job, i) => (
                <View style={dstyles.text} key={i}>
                  <Text style={[dstyles.textWhite, dstyles.textCenter]}>{job.startDate + ' - ' + job.endDate}</Text>
                  <Text style={[dstyles.text, dstyles.textWhite, dstyles.textCenter, dstyles.textBold]}>{job.name + ' in ' + job.companyName}</Text>
                  <Button type="solid" title={label.delete} buttonStyle={[dstyles.button3, dstyles.bgRed]} onPress={() => deleteJob(i)} />
                </View>
              ))
            }</View>
            <View style={[dstyles.row, dstyles.alignMiddle, {marginTop: Size.md}]}>
              <Button type="solid" title={label.addJob} buttonStyle={[dstyles.button2, dstyles.bgSecondary, {width: 150}]} onPress={addAJob} />
            </View>
          </View>
        </View>
        <View style={[dstyles.content, dstyles.buttons]}>
          <View style={[dstyles.row, dstyles.alignMiddle]}>
            <Button type="solid" title={label.save} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={savePressed} />
          </View>
        </View>
      </ScrollView>
    </Layout>)
  );
}



function EditCompanyProfile(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [msgTitle, setMsgTitle] = useState(false);
  const [msg, setMsg] = useState(false);
  const [loading, setLoading] = useState(true);
  const [loadingText, setLoadingText] = useState('');
  const [first, setFirst] = useState(true);
  const [updating, setUpdating] = useState(false);
  const [errors, setErrors] = useState([]);
  const [image, setImage] = useState(null);
  const height = 1020;
  const id = params && params.id ? params.id : null;
  const docDbRef = firebase.firestore().collection('businesses');
  const editCallback = params && params.callback ? params.callback : null;

  const refresh = () => {
    setLoading(true);
    setFirst(true);
  }

  // validate inputs
  const validateInputs = function() {
    var errs = [];
    if (props.cprofileRed.cprofile.name == '') {
      errs.push('Company Name cannot be empty.');
    }
    if (props.cprofileRed.cprofile.email == '') {
      errs.push('Company Email cannot be empty.');
    } else if (!/\w+@\w+\.\w+/.test(props.cprofileRed.cprofile.email)) {
      errs.push('Company Email format is wrong.');
    }
    return errs;
  }

  const saveCompanyProfile = async function() {
    var errs = validateInputs();

    if (errs.length > 0) {
      setMsgTitle('Save Failed');
      setMsg('\n'+errs.join('\n'));
      setErrors(errs);
    } else {
      var docData = null, docId = null;

      let userData = await GlobalFunc.getUser({self: true});

      // read user data
      await docDbRef.doc(userData.id).get().then(res => {
        docId = res.id;
        docData = res.data();
      }).catch(error => { console.log('Error: ' + error); });

      // handle image
      if (image && image.uri) { // if uploaded a new image
        console.log('uploading image');
        const uri = image.uri;
        const filename = auth().currentUser.uid + '_' + image.fileName;
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        const task = storage().ref(filename).putFile(uploadUri);
        try {
          await task;
        } catch (e) {
          console.error(e);
        }

        docData.profile_picture = FB_URL + '' + filename;
      }

      // save user data
      // set
      docData.name = props.cprofileRed.cprofile.name;
      docData.occupation = props.cprofileRed.cprofile.occupation;
      docData.year = props.cprofileRed.cprofile.year;
      docData.address = props.cprofileRed.cprofile.address;
      docData.email = props.cprofileRed.cprofile.email;
      docData.contact_number = props.cprofileRed.cprofile.telp;
      docData.facebook_url = props.cprofileRed.cprofile.facebook;
      docData.instagram = props.cprofileRed.cprofile.instagram;
      docData.linkedin = props.cprofileRed.cprofile.linkedin;

      // write
      await docDbRef.doc(docId).set(docData).then(docRef => {
        setUpdating(false);
        setMsgTitle('');
        setMsg(label.profileSaved);
      }).catch(error => {
        console.log('Error: ' + error);
        setUpdating(false);
        setMsgTitle('');
        setMsg(label.profileFailed);
      });

      setErrors(false);

      if (typeof editCallback != undefined) {
        editCallback();
      }
    }
  }

  const callback = function() {
    setMsg(false);
    setLoadingText('');
    if (!errors) navigation.goBack();
  }

  const savePressed = function() {
    setLoadingText(label.updating);
    setUpdating(true);
  }

  const handleUploadPhoto = async (res) => {
    setImage(res);
    return true;
  }

  useEffect(function() {
    if (loading && first) {
      setFirst(false);
      async function setUserData() {
        let userData = await GlobalFunc.getUser({self: true});
        var selfData = {};

        if (userData) { // if user is logged in
          selfData = await GlobalFunc.getUser({userId: userData.id});
        }

        // props.setProfile({name: 'PT. Photoshoot', profpic:img.woman, year: '2016', email: 'info@photoshoot.com', telp: '081232343233', address: 'Surabaya, Indonesia', facebook: 'facebook.com/test', instagram: 'test ID', linkedin: 'linkedin.com/test'});
        props.setProfile(selfData);
        setLoading(false);
      }
      setUserData();
    }
  });

  return (
    loading ?
    <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.app} />
      <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{label.loading}</Text></View>
    </View> :
    (updating ? <View style={dstyles.container}>
      <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{loadingText}</Text></View>
    </View> :
    <Layout style={dstyles.container} height={height} msg={msg} callback={callback}>
      <CustomHeader navigation={navigation} title={label.profile} options={{back: true, menu: false}} />
      <ScrollView style={dstyles.scrollViewContent}>
        <View style={dstyles.bgTheme}>
          <View style={[dstyles.tightContent, {paddingTop: Size.lg, paddingBottom: Size.xl}]}>
            <ImageInput source={{uri: props.cprofileRed.cprofile.profpic}} title={label.image} circle={true} onChange={(e) => props.setProfileImage(e)} />
            <Text style={[dstyles.pageTitle, dstyles.textWhite]}>{props.cprofileRed.cprofile.name}</Text>
            <LabelInput title={label.name + "*"} value={props.cprofileRed.cprofile.name} placeholder={label.inputName} type={2} onChange={(e) => props.setProfileName(e)} />
            <LabelInput title={label.year} value={props.cprofileRed.cprofile.year} placeholder={label.inputYear} type={2} onChange={(e) => props.setProfileYear(e)} />
            <LabelInput title={label.email + "*"} value={props.cprofileRed.cprofile.email} placeholder={label.inputEmail} type={2} onChange={(e) => props.setProfileEmail(e)} />
            <LabelInput title={label.telp} value={props.cprofileRed.cprofile.contact_number} placeholder={label.inputTelp} type={2} onChange={(e) => props.setProfileTelp(e)} />
            <LabelInput title={label.address} value={props.cprofileRed.cprofile.address} placeholder={label.inputAddress} type={2} onChange={(e) => props.setProfileAddress(e)} />
            <LabelInput title={label.fb} value={props.cprofileRed.cprofile.facebook} placeholder={label.inputFb} type={2} onChange={(e) => props.setProfileFacebook(e)} />
            <LabelInput title={label.insta} value={props.cprofileRed.cprofile.instagram} placeholder={label.inputInsta} type={2} onChange={(e) => props.setProfileInstagram(e)} />
            <LabelInput title={label.linkedIn} value={props.cprofileRed.cprofile.linkedin} placeholder={label.inputLinkedIn} type={2} onChange={(e) => props.setProfileLinkedin(e)} />
          </View>
        </View>
        <View style={[dstyles.content, dstyles.buttons]}>
          <View style={[dstyles.row, dstyles.alignMiddle]}>
            <Button type="solid" title={label.save} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={saveCompanyProfile} />
          </View>
        </View>
      </ScrollView>
    </Layout>)
  );
}

const styles = StyleSheet.create({
});

const mapStateToProps = (state) => {
  const { profileRed } = state
  return { profileRed }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    setProfile,
    setProfileName,
    setProfileImage,
    setProfileEmail,
    setProfileOccupation,
    setProfileGrad,
    setProfileAddress,
    setProfileFacebook,
    setProfileInstagram,
    setProfileLinkedin,
  }, dispatch)
);

const mapStateToProps2 = (state) => {
  const { cprofileRed } = state
  return { cprofileRed }
};

const mapDispatchToProps2 = dispatch => (
  bindActionCreators({
    setProfile,
    setProfileName,
    setProfileImage,
    setProfileEmail,
    setProfileOccupation,
    setProfileYear,
    setProfileAddress,
    setProfileFacebook,
    setProfileInstagram,
    setProfileLinkedin,
  }, dispatch)
);

export const EditProfileScreen = connect(mapStateToProps, mapDispatchToProps)(EditProfile);

export const EditCompanyProfileScreen = connect(mapStateToProps2, mapDispatchToProps2)(EditCompanyProfile);

import React, { useState } from 'react';
import { ScrollView, StyleSheet, TextInput, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements';
import { StackActions } from '@react-navigation/native';
import Layout from '@layout/basic';
import { Color, Input, Size, dstyles, label } from '@handler/data';
import { img } from '@handler/img';
import firebase from "../../config/firebase";
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-community/async-storage';


export default function LogoutScreen(props) {
  const params = props.route.params;
  const navigation = props.navigation;
  const height = 550;

  async function logout() {
    try {
      await AsyncStorage.setItem('logged_in_user', JSON.stringify({}));
    } catch (error) {
      console.log("Error Save Session: " + error.message);
      setMsg(label.errorMsgLogout);
    }
    auth().signOut().then(() => {
      console.log('User signed out!');
    }).catch(err => {
      console.log('Error: ' + err);
    });
    navigation.dispatch(StackActions.replace('Posters'));
  }

  logout();

  return (
    <View style={[dstyles.container, dstyles.bgTheme]}>
      <View style={dstyles.fullScreen}>
        <Image source={img.logo} style={dstyles.logo} />
      </View>
      <View style={styles.textCont}>
        <Text style={styles.text}>{'Logging out...'}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  logoCont: {
    position: 'absolute',
    top: 70,
    alignSelf: 'center',
    zIndex: 2,
  },
  logo: {
    width: 90,
    height: 90,
  },
  // logoCont: {
  //   flex: 1,
  //   flexDirection: 'row',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   height: 260,
  // },
  back: {
    minWidth: "100%",
    height: 260,
    zIndex: 1,
  },
  textCont: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'center',
    alignItems: 'center', // horizontal align
    justifyContent: 'center',
    marginTop: 0,
    width: 300,
    padding: Size.lg,
  },
  text: {
    fontSize: 14,
    color: '#ffffff'
  }
});

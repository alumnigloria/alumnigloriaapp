import React, { useState } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Button, Text } from 'react-native-elements';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Layout from '@layout/basic';
import { icons, img } from '@handler/img';
import Banner from '@elements/banner';
import CustomHeader from '@elements/header';
import { ImageInput, LabelInput, TextAreaInput } from '@elements/input';
import { Color, Size, dstyles, label } from '@handler/data';
import GlobalFunc from '@handler/functions';
import firebase, { FB_URL } from "../../config/firebase";
import auth from '@react-native-firebase/auth';
import storage from '@react-native-firebase/storage';
import AsyncStorage from '@react-native-community/async-storage';
import { setPromo, setPromoName, setPromoImage, setPromoContact, setPromoDetails, setPromoUrl, setPromoLocation, setPromoStartTime, setPromoEndTime } from '@actions/promoActions';


function EditPromotionScreen(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [user, setUser] = useState({});
  const [adding, setAdding] = useState(false);
  const [updating, setUpdating] = useState(false);
  const [deleting, setDeleting] = useState(false);
  const [loading, setLoading] = useState(true);
  const [loadingText, setLoadingText] = useState('');
  const [msgTitle, setMsgTitle] = useState(false);
  const [msg, setMsg] = useState(false);
  const [errors, setErrors] = useState([]);
  const [image, setImage] = useState(null);
  const [prom, setProm] = useState(null);
  //const [uploading, setUploading] = useState(false);
  //const [transferred, setTransferred] = useState(0);
  const height = 940;
  const id = params && params.id ? params.id : null;
  const docDbRef = firebase.firestore().collection('promotions');
  const imgRef = firebase.firestore().collection('images');
  const editCallback = params && params.callback ? params.callback : null;

  // validate inputs
  const validateInputs = function() {
    var errs = [];
    if (props.promoRed.promo.name == '') {
      errs.push('Promotion Name cannot be empty.');
    }
    if (props.promoRed.promo.contact == '') {
      errs.push('Promotion Email cannot be empty.');
      } else if (!/\w+@\w+\.\w+/.test(props.promoRed.promo.contact)) {
        errs.push('Promotion Email format is wrong.');
      }
    if (props.promoRed.promo.location == '') {
      errs.push('Promotion Location cannot be empty.');
    }
    if (props.promoRed.promo.start_time == '') {
      errs.push('Promotion Start Time cannot be empty.');
    }
    if (props.promoRed.promo.details == '') {
      errs.push('Promotion Details cannot be empty.');
    }
    // if (image == null) {
    //   errs.push('Promo Picture cannot be empty.');
    // }
    return errs;
  }

  // save promo
  const savePromotion = async () => {
    var errs = validateInputs();

    if (errs.length > 0) { // Error
      setMsgTitle('Save Failed');
      setMsg('\n'+errs.join('\n'));
      setErrors(errs);

    } else { // Save
      var docData = null, docId = null;

      // read promo data
      await docDbRef.doc(id).get().then(res => {
        docId = res.id;
        docData = res.data();
      }).catch(error => { console.log('Error: ' + error); });

      // handle image
      if (image && image.uri) { // if uploaded a new image
        console.log('uploading image');
        const uri = image.uri;
        const filename = auth().currentUser.uid + '_' + image.fileName; //uri.substring(uri.lastIndexOf('/') + 1);
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        // setTransferred(0);
        const task = storage().ref(filename).putFile(uploadUri);
        // set progress state
        // task.on('state_changed', snapshot => {
        //   setTransferred(
        //     Math.round(snapshot.bytesTransferred / snapshot.totalBytes) * 10000
        //   );
        // });
        try {
          await task;
        } catch (e) {
          console.error(e);
        }

        var img = null, imgId = null;

        // get image
        await imgRef.get().then(snapshot => {
          snapshot.forEach((res2) => {
            var imgdata = res2.data();
            if (imgdata.ref_id == 'promotions/' + docId) {
              imgId = res2.id;
              img = imgdata;
              return false;
            }
          });
        }).catch(error => { console.log('Error: ' + error); });

        // edit image
        try {
          img.image = FB_URL + '' + filename;

          // save image
          await imgRef.doc(imgId).set(img).then(ret => {
            console.log('Image saved.');
          }).catch(error => { console.log('Error: ' + error); });
        } catch(err) {
          console.log('Image not found.');
        }
      }

      // save promo data
      // set
      docData.contact = props.promoRed.promo.contact;
      docData.description = props.promoRed.promo.details;
      docData.end_time = props.promoRed.promo.end_time;
      docData.is_approved = false;
      docData.location = props.promoRed.promo.location;
      docData.name = props.promoRed.promo.name;
      docData.promotion_url = props.promoRed.promo.url;
      docData.sequence = '';
      docData.start_time = props.promoRed.promo.start_time;

      // write
      await docDbRef.doc(id).set(docData).then(docRef => {
        setUpdating(false);
        setMsgTitle('');
        setMsg(label.promoSaved);
      }).catch(error => {
        console.log('Error: ' + error);
        setUpdating(false);
        setMsgTitle('');
        setMsg(label.promoSaveFailed);
      });

      setErrors(false);

      if (typeof editCallback != undefined) {
        editCallback();
      }
    }
  }

  // add promo
  const addPromotion = async () => {
    var errs = validateInputs();

    if (errs.length > 0) { // Error
      setAdding(false);
      setMsgTitle('Add Failed');
      setMsg('\n'+errs.join('\n'));
      setErrors(errs);

    } else { // Add
      let userData = await GlobalFunc.getUser({self: true});

      var docData = {
        business_owner_id: '',
        contact: props.promoRed.promo.contact,
        description: props.promoRed.promo.details,
        end_time: props.promoRed.promo.end_time,
        is_active: true,
        is_approved: false,
        is_featured: false,
        location: props.promoRed.promo.location,
        name: props.promoRed.promo.name,
        promotion_url: props.promoRed.promo.url,
        sequence: '',
        start_time: props.promoRed.promo.start_time,
        user_owner_id: 'users/' + userData.id,
      };

      var msg = '', docId = null;

      // write
      await docDbRef.add(docData).then(docRef => {
        msg = label.promoAdded;
        docId = docRef.id;
      }).catch(error => {
        console.log('Error: ' + error);
        msg = label.promoAddFailed;
      });

      // handle image
      if (image && image.uri) {
        console.log('uploading image');
        const uri = image.uri;
        const filename = auth().currentUser.uid + '_' + (image.fileName ? image.fileName : image.uri.substr(image.uri.lastIndexOf('/') + 1));
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        const task = storage().ref(filename).putFile(uploadUri);
        try {
          await task;
        } catch (e) {
          console.error(e);
        }

        // create image
        var img;
        if (!prom) {
          img = {
            image: FB_URL + '' + filename,
            name: filename,
            ref_id: 'promotions/' + docId,
            community_id: '',
            promotion_id: "promotions/" + docId,
            created_uid: 'users/' + userData.id,
            created_time: moment(),
            last_updated_uid: 'users/' + userData.id,
            last_updated_time: moment(),
            sequence: 1,
          }
        } else {
          img = {
            image: FB_URL + '' + filename,
            name: filename,
            ref_id: 'promotions/' + docId,
            community_id: '',
            promotion_id: "promotions/" + docId,
            last_updated_uid: 'users/' + userData.id,
            last_updated_time: moment(),
            sequence: 1,
          }
        }

        // save image
        await imgRef.add(img).then(ret => {
          console.log('Image saved.');
        }).catch(error => { console.log('Error: ' + error); });
      }

      setAdding(false);
      setMsgTitle('');
      setMsg(msg);
      setErrors(false);

      if (typeof editCallback != undefined) {
        editCallback();
      }
    }
  }

  // delete promo
  const deletePromotion = async () => {
    console.log('Deleting data');
    var msg = '';
    var img = props.promoRed.promo.image && props.promoRed.promo.image.uri.includes('http') ? props.promoRed.promo.image.uri : null;
    let userData = await GlobalFunc.getUser({self: true});
    var imgData = await GlobalFunc.getImage({userId: userData.id, refId: 'promos/' + params.id});

    // delete promo
    await docDbRef.doc(id).delete().then(docRef => {
      msg = label.promoDeleted;
    }).catch(error => {
      console.log('Error: ' + error);
      msg = label.promoDeleteFailed;
    });

    // delete img
    if (imgData) {
      await imgRef.doc(imgData.id).delete().then(docRef => {
      }).catch(error => {
        console.log('Error: ' + error);
      });
    }

    setDeleting(false);
    setMsgTitle('');
    setMsg(msg);
    setErrors(false);

    if (typeof editCallback != undefined) {
      editCallback();
    }
  }

  const handleUploadPhoto = async (res) => {
    setImage(res);
    return true;

    // Local Server
    // fetch(uploadServer, {
    //   method: 'POST',
    //   body: createFormData(val, { userId: '123' }),
    // })
    // .then((response) => response.json())
    // .then((response) => {
    //   console.log('upload succes', response);
    //   alert('Upload success!');
    //   //setVal(null);
    // })
    // .catch((error) => {
    //   console.log('upload error', error);
    //   alert('Upload failed!');
    // });
  }

  const callback = function() {
    setMsg(false);
    setLoadingText('');
    if (!errors) navigation.goBack();
  }

  const addPressed = function() {
    setLoadingText(label.adding);
    setAdding(true);
  }

  const savePressed = function() {
    setLoadingText(label.updating);
    setUpdating(true);
  }

  const deletePressed = function() {
    setLoadingText(label.deleting);
    setDeleting(true);
  }

  var buttons;
  if (params && params.id) {
    if (loading) {
      async function setPromoData() {
        let userData = await GlobalFunc.getUser({self: true});
        var promoData = {};

        if (userData) { // if user is logged in
          promoData = await GlobalFunc.getPromo({userId: userData.id, promoId: params.id});
        }

        setLoading(false);
        setUser(userData);

        if (promoData) {
          setProm(promoData);
          props.setPromo(promoData);
        }
      }
      setPromoData();
    } else if (updating) {
      savePromotion();
    } else if (deleting) {
      deletePromotion();
    }

    buttons =
      <View style={dstyles.buttons}>
        <View style={[dstyles.row, dstyles.alignMiddle]}>
          <Button type="solid" title={label.save} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={savePressed} />
        </View>
        <View style={[dstyles.row, dstyles.alignMiddle]}>
          <Button type="solid" title={label.delete} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={deletePressed} />
        </View>
      </View>;

  } else {
    var title = label.newPromo;

    if (loading) {
      props.setPromo({name: '', image: null, contact: '', details: '', url: ''});
      setLoading(false);
    } else if (adding) {
      addPromotion();
    }

    buttons =
      <View style={dstyles.buttons}>
        <View style={[dstyles.row, dstyles.alignMiddle]}>
          <Button type="solid" title={label.addPromo} buttonStyle={[dstyles.button2, dstyles.bgSecondary, {width: 150}]} onPress={addPressed} />
        </View>
      </View>;
  }

  var accepted = prom && prom.id ? 'status: ' + (props.promoRed.promo.is_approved ? 'approved.' : 'not yet approved.') : null;

  return (
    loading ? <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.myep} options={{back: true, menu: false}} />
      <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{label.loading}</Text></View>
    </View> :
    (updating || adding || deleting ? <View style={dstyles.container}>
      <View style={[dstyles.fullScreen, dstyles.bgTheme]}><Text style={[dstyles.loading, dstyles.textWhite]}>{loadingText}</Text></View>
    </View> :
    <Layout style={dstyles.container} height={height} title={msgTitle} msg={msg} callback={callback}>
      <CustomHeader navigation={navigation} title={label.myep} options={{back: true, menu: false}} />
      <ScrollView style={dstyles.scrollViewContent}>
        <Banner title={title ? title : props.promoRed.promo.name}></Banner>
        <View style={dstyles.tightContent}>
          <Text style={[dstyles.text, dstyles.textRight, {marginBottom: 20}]}>{accepted}</Text>
          <LabelInput title={label.name + "*"} value={props.promoRed.promo.name} placeholder={label.inputName} onChange={(e) => props.setPromoName(e)} />
          <ImageInput source={props.promoRed.promo.image ? props.promoRed.promo.image : img.empty_add} title={label.image} onChange={(e) => props.setPromoImage(e)} width={600} height={450} handleUploadPhoto={handleUploadPhoto} />
          <LabelInput title={label.email + "*"} value={props.promoRed.promo.contact} desc={label.exampleEmail} placeholder={label.inputEmail} onChange={(e) => props.setPromoContact(e)} />
          <LabelInput title={label.url} value={props.promoRed.promo.url} desc={label.exampleUrl} placeholder={label.inputURL} onChange={(e) => props.setPromoUrl(e)} />
          <LabelInput title={label.location + "*"} value={props.promoRed.promo.location} desc={label.exampleLocation} placeholder={label.inputLocation} onChange={(e) => props.setPromoLocation(e)} />
          <LabelInput title={label.startTime + "*"} value={props.promoRed.promo.start_time} desc={label.exampleTime} placeholder={label.inputStartTime} onChange={(e) => props.setPromoStartTime(e)} />
          <LabelInput title={label.endTime} value={props.promoRed.promo.end_time} desc={label.exampleTime} placeholder={label.inputEndTime} onChange={(e) => props.setPromoEndTime(e)} />
          <TextAreaInput title={label.details + "*"} value={props.promoRed.promo.details} placeholder={label.input} onChange={(e) => props.setPromoDetails(e)} />
          { buttons }
        </View>
      </ScrollView>
    </Layout>
    )
  );
}

const styles = StyleSheet.create({
});

const mapStateToProps = (state) => {
  const { promoRed } = state
  return { promoRed }
};

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    setPromo,
    setPromoName,
    setPromoImage,
    setPromoContact,
    setPromoUrl,
    setPromoLocation,
    setPromoStartTime,
    setPromoEndTime,
    setPromoDetails,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(EditPromotionScreen);

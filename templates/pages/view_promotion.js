import React, { useState } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Button, Image, Text } from 'react-native-elements';
import { icons, img } from '@handler/img';
import BorderedImage from '@elements/image';
import CustomHeader from '@elements/header';
import { Color, Size, dstyles, label } from '@handler/data';
import GlobalFunc from '@handler/functions';
import QRCode from 'react-native-qrcode-svg';
import firebase from "../../config/firebase";
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';


export default function ViewPromotionScreen(props) {
  const navigation = props.navigation;
  const name = props.route.name;
  const params = props.route.params;
  const [user, setUser] = useState({});
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(true);

  if (loading) {
    async function setPromoData() {
      let userData = await GlobalFunc.getUser({self: true});
      var promoData = {};

      if (userData) { // if user is logged in
        promoData = await GlobalFunc.getPromo({userId: userData.id, promoId: params.id});
      }

      setLoading(false);
      setUser(userData);

      if (promoData) setData(promoData);
    }
    setPromoData();
  }

  function registerPromotion() {
    console.log('Register Promotion.');
    // ... implement here
    navigation.goBack();
  }

  return (
    <View style={dstyles.container}>
      <CustomHeader navigation={navigation} title={label.app} options={{back: true, menu: false}} />
      {
        loading ? <Text style={dstyles.loading}>{label.loading}</Text> :
        <ScrollView style={dstyles.scrollViewContent}>
          <View style={dstyles.bgTheme}>
            <View style={dstyles.tightContent}>
              <Text style={[dstyles.pageTitle, dstyles.textWhite]}>{data.name}</Text>
              <BorderedImage source={data.image} title={label.image} />
              <Text style={[dstyles.subtitle, dstyles.textWhite, dstyles.textLeft]}>{label.description}</Text>
              <Text style={dstyles.textWhite}>{data.details}</Text>
              <Text style={[dstyles.subtitle, dstyles.textWhite, dstyles.textLeft]}>{label.date}</Text>
              <Text style={dstyles.textWhite}>{data.startDate && data.endDate ? moment(data.startDate).format('DD MMM YYYY') + ' - ' + moment(data.endDate).format('DD MMM YYYY') : '-'}</Text>
              <Text style={[dstyles.subtitle, dstyles.textWhite, dstyles.textLeft]}>{label.location}</Text>
              <Text style={dstyles.textWhite}>{data.location}</Text>
              <View style={dstyles.qrCodeCont}>
                <QRCode value={data.url} size={140} />
              </View>
            </View>
          </View>
          <View style={[dstyles.content, dstyles.buttons, {marginBottom: Size.lg}]}>
            <View style={[dstyles.row, dstyles.alignMiddle]}>
              <Button type="solid" title={label.back} buttonStyle={[dstyles.button2, dstyles.bgSecondary]} onPress={() => navigation.goBack()} />
            </View>
          </View>
        </ScrollView>
      }
    </View>
  );
}

const styles = StyleSheet.create({
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

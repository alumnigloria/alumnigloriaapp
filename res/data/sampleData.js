const date = new Date();
const dateString = date.toISOString().slice(0,10);

export const profile = [
  {"_id": "5ebd5f2d8a999fc2083fdef5", "access": 1, "address": {"street":"21-23 Howe Street", "city":"Murrumbeena", "country":"Australia"}, "age": 24, "email": "gabrielandreas96@gmail.com", "phone": "0423746455", "full_name": "Gabriel Andreas", "friends": [], "socmed": {"facebook": "", "instagram": "", "linkedin": "https://linkedin.com", "line": "", "whatsapp": ""}, "user_id": "5ec3ba657feafc09549844db", "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg"}
];

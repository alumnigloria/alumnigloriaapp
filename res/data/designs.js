import { Dimensions } from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const colors = {
  theme: '#F7931D',
  themeDark: '#B06001',
  text: '#000000',
  white: '#ffffff',
  lighterGrey: '#f0f0f0',
  lightGrey: '#eaeaea',
  grey: '#aaaaaa',
  darkGrey: '#888888',
  dark: '#333333',
  secondary: '#298DD7',
  transparent: 'rgba(0,0,0,.5)',
  red: '#cc4433'
}

export const sizes = {
  fxsmall: 10,
  fsmall: 11,
  fnormal: 12,
  fmedium: 15,
  flarge: 18,
  fxlarge: 21,
  fwxxlight: "100",
  fwxlight: "200",
  fwlight: "300",
  fwreg: "400",
  fwmed: "500",
  fwbold: "bold",
  fwxbold: "700",
  fwblack: "800",
  fwxblack: "900",
  xs: 5,
  sm: 10,
  md: 15,
  lg: 20,
  xl: 30,
  xxl: 40,
  pgBannerH: 110,
  rad: 20,
  screenWidth: windowWidth,
  screenHeight: windowHeight,
}

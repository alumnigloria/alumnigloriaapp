import {Platform} from 'react-native';

export const autoComplete = {
  default: 'off',
  username: 'username',
  email: 'email',
  name: 'name',
  phone: 'tel',
  address: 'street-address',
}

export const keyboard = {
  default: 'default',
  number: 'number-pad',
  email: 'email-address',
  phone: 'phone-pad',
  url: 'url',
  namephone: 'name-phone-pad',
  password: 'visible-password'
}

export const input = {
  default: 'none',
  url: 'URL',
  email: 'emailAddress',
  name: 'name',
  phone: 'telephoneNumber',
  username: 'username',
  password: 'password',
  number: 'number'
}

export const inputTypes = {
  none: {name:input.default},
  url: {name:input.url, keyboard: (Platform.ios ? keyboard.url : keyboard.default), strict: 'nospace'},
  address: {name:input.default, autoComplete: autoComplete.street, length: 100},
  email: {name:input.email, keyboard: keyboard.email, strict: 'email', autoComplete: autoComplete.email, length: 100},
  name: {name:input.name, capital: 'words', autoComplete: autoComplete.name},
  phone: {name:input.phone, keyboard: keyboard.phone, strict: 'phone', autoComplete: autoComplete.phone, length: 15},
  username: {name:input.username, strict: 'nospace', autoComplete: autoComplete.username, length: 20},
  password: {name:input.password, keyboard: (Platform.android ? keyboard.password : keyboard.default), strict: 'nospace', length: 20},
  number: {name:input.number, keyboard: keyboard.numeric, strict: 'number'}
}

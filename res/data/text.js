export const text = {
  add: 'Add',
  adding: 'Adding new data...',
  addCommunity: 'Add Community',
  addPromo: 'Add Promo',
  addJob: 'Add A Job',
  address: 'Address',
  all: 'All',
  app: 'Alumni',
  back: 'Back',
  call: 'Call',
  company: 'Company',
  companyName: 'Company',
  contact: 'Contact',
  date: 'Date',
  delete: 'Delete',
  deleting: 'Deleting data...',
  details: 'Details',
  description: 'Description',
  edit: 'Edit',
  email: 'Email',
  emailOrPhone: 'Enter an email address or a phone number',
  emailSample: 'test@test.com',
  endTime: 'End Time',
  endYear: 'End Year',
  errorMsgCEmailExist: 'Company with that email is already exists!',
  errorMsgCUserRegisterFailed: 'Company Registration Failed',
  errorMsgFirstNameEmpty: 'First Name cannot be empty.',
  errorMsgGeneral: 'Something is wrong.',
  errorMsgLastNameEmpty: 'Last Name cannot be empty.',
  errorLoadingData: 'Error Loading Data',
  errorMsgEmailExist: 'User with that email is already exists!',
  errorMsgEmailUsed: 'That email address is already in use.',
  errorMsgEmailInvalid: 'That email address is invalid.',
  errorMsgEmailFormat: 'Email format is wrong.',
  errorMsgGradEmpty: 'Graduation Year cannot be empty.',
  errorMsgLocationEmpty: 'Location cannot be empty.',
  errorMsgLoginUPEmpty: 'Please enter your registered username and password.',
  errorMsgLoginUPWrong: 'Your username or password is wrong.\nPlease contact Gloria to reset password.',
  errorMsgLogout: 'Logout failed. Please try again.',
  errorMsgNameEmpty: 'Name cannot be empty.',
  errorMsgNotActivated: 'User is not activated yet.\nPlease contact Gloria to activate the account.',
  errorMsgNetwork: 'A network error has occurred, please try again.',
  errorMsgPasswordEmpty: 'Password cannot be empty.',
  errorMsgPasswordInvalid: 'Password is invalid!',
  errorMsgPasswordReq: 'Password length must be more than 6, contains at least 1 letter, 1 number & 1 special character.',
  errorMsgPasswordReq2: 'That password must contains 6 characters.',
  errorMsgTooManyAttempts: 'Access to this account has been temporarily disabled due to many failed login attempts.',
  errorMsgUserRegisterFailed: 'Member Registration Failed',
  errorMsgUnregisteredEmail: 'Email is not registered yet.',
  errorMsgYearEmpty: 'Established Year cannot be empty.',
  establishedYear: 'Established Year',
  communities: 'Communities',
  communityAdded: 'An Community Successfully Added!',
  communityAddFailed: 'Adding An Community Failed!',
  communityDeleted: 'Community Deleted!',
  communityDeleteFailed: 'Community Delete Failed!',
  communitySaved: 'Community Saved!',
  communitySaveFailed: 'Community Save Failed!',
  exampleEmail: 'E.g. test@test.com',
  exampleLocation: 'E.g. Jakarta, Indonesia',
  exampleTime: 'E.g. 12 August 2021 14:00',
  exampleUrl: 'E.g. https://www.google.com',
  first_name: 'First Name',
  fb: 'Facebook URL',
  grad: 'Graduate Year',
  image: 'Image',
  insta: 'Instagram ID',
  input: 'Type here...',
  inputAddress: 'Type your address here...',
  inputCompanyName: 'Type your company name here...',
  inputEmail: 'e.g. test@test.com',
  inputEndDate: 'Type the end date here...',
  inputEndTime: 'Type the end time here...',
  inputFb: 'Type your facebook URL here...',
  inputFirstName: 'e.g. John',
  inputGrad: 'e.g. 2020 ',
  inputInsta: 'Type your instagram ID here...',
  inputJob: 'Type your job name here...',
  inputLastName: 'e.g. Doe',
  inputLinkedIn: 'Type your linkedIn URL here...',
  inputLocation: 'Type your location here...',
  inputName: 'Type your name here...',
  inputOccupation: 'Type your occupation here...',
  inputPassword: 'Type your password here...',
  inputStartDate: 'Type the start date here...',
  inputStartTime: 'Type the start time here...',
  inputTelp: 'e.g. 08112341234',
  inputURL: 'Type the URL here...',
  inputYear: 'Type your established year here...',
  job: 'Job',
  jobs: 'Jobs',
  jobAdded: 'A job successfully added!',
  jobAddFailed: 'Adding a job failed!',
  last_name: 'Last Name',
  linkedIn: 'LinkedIn URL',
  loading: 'Loading data...',
  location: 'Location',
  login: 'Login',
  loremipsum: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.',
  mycommunities: 'My Communities',
  mypromos: 'My Promotions',
  myep: 'My Communities & Promotions',
  name: 'Name',
  noJob: 'No job yet',
  newCommunity: 'New Community',
  newPromo: 'New Promotion',
  noCommunities: 'No Communities',
  noDesc: 'No description.',
  noPromos: 'No Promotions',
  occupation: 'Occupation',
  ok: 'OK',
  password: 'Password',
  people: 'People',
  profile: 'Profile',
  profileSaved: 'Profile Saved!',
  cProfileSaved: 'Company Profile Saved!',
  promotions: 'Promotions',
  promoAdded: 'A Promo Successfully Added!',
  promoAddFailed: 'Adding A Promo Failed!',
  promoDeleted: 'Promotion Deleted!',
  promoDeleteFailed: 'Promotion Delete Failed!',
  promoSaved: 'Promotion Saved!',
  promoSaveFailed: 'Promotion Save Failed!',
  register: 'Register',
  registerSuccess: 'Registration Success!',
  registerFailed: 'Registration Failed!',
  save: 'Save',
  search: 'Search',
  search_custom: 'Custom Search',
  seemore: 'See more...',
  settings: 'Settings',
  signin: 'Sign In',
  signup: 'Sign Up here',
  startTime: 'Start Time',
  startYear: 'Start Year',
  telp: 'Telephone',
  telpSample: '08123456789',
  test: 'Test',
  title: 'Title',
  type: 'Type',
  updating: 'Updating data...',
  uploadingImage: 'Uploading Image...',
  url: 'URL',
  welcome: 'Welcome',
  welcomeCompany: 'Company Login',
  welcomeMember: 'Member Login',
  welcomeb: 'Welcome Back,',
}
